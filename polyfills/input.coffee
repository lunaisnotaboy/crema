# Backport input event to IE 8 and older
#
#= require jquery

supportsInputEvent = 'oninput' of document.createElement 'input'

# IE 8 and older
if !supportsInputEvent and document.attachEvent?
  # Setup key event handlers when the field is focused.
  $(document).on 'focus', 'input, textarea', (event) ->
    field = event.currentTarget

    onPropertyChange = ->
      $(field).trigger 'input'
      return

    onBlur = ->
      field.detachEvent 'onpropertychange', onPropertyChange
      field.detachEvent 'onblur', onBlur
      return

    field.attachEvent 'onpropertychange', onPropertyChange
    field.attachEvent 'onblur', onBlur

    return
