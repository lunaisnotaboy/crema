# Position Sticky polyfill
#
#= require jquery
#= require sprockets-modernizr/css-positionsticky
#= require crema/events/pageupdate
#
#
# Remember to include the associated css file
#
# ``` css
# /*= require crema/polyfills/sticky */
# ```
#
# Or
#
# ``` css
# @import "crema/polyfills/sticky";
# ```
#

# Skip if browser supports position:sticky
return if Modernizr.csspositionsticky

install = (el) ->
  $el = $ el
  return if $el.data 'sticky-installed'

  top = $el.offset().top

  # offsetParent maybe be null if its display:none
  return unless container = el.offsetParent
  containerTop = $(container).offset().top
  containerHeight = $(container).height()

  document.addEventListener 'scroll', ->
    scrollY = window.scrollY

    # Annoying, but we can't rely on caching these values. Positions
    # and dimensions can change as the the page is interacted with.
    offsetBottom = containerTop + containerHeight - $el.height()

    # If scrolled pasted top of sticky element and its bottom isn't
    # already at the bottom of the document.
    if offsetBottom > top and scrollY > top
      $el.addClass 'stick'

      # If we've scrolled to the point where the bottom of the element
      # now reaches the bottom of the page, apply bottom:0.
      if offsetBottom < scrollY
        $el.addClass 'stick-bottom'
      else
        $el.removeClass 'stick-bottom'

    # Else if scrolled is above the sticky element,
    # go back to position:absolute
    else
      $el.removeClass 'stick stick-bottom'

    return

  $el.data 'sticky-installed', true

$.pageUpdate installSticky = ->
  for el in $ '.sticky'
    install el
  return
