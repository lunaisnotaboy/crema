# Relative Date Filter
#
#= provides js-relative-date
#
#= require jquery
#= require moment/moment
#= require crema/events/pageupdate
#
# Rewrites timestamps as dynamic relative ones.
#
#     less than a minute ago
#     about a month ago
#     2 years ago
#
# ### Markup
#
# A `time` element with the class name `js-relative-date` must be used
# as a wrapper. The datetime string should be in ISO 8601 as per the
# HTML5 spec. The contents of the element will be replaced. But its
# still good to provide a decent fallback.
#
# ``` html
# <time class="js-relative-date" datetime="<%= created_at.iso8601 %>">
#   <%= created_at.to_date %>
# </time>
# ```
#

refreshRelativeDates = (container = document) ->
  for el in $(container).find '.js-relative-date' when $(el).attr('datetime')
    if date = moment $(el).attr('datetime'), 'YYYY-MM-DDTHH:mm:ssZ'
      text = date.fromNow()
      text = "just now" if text is "a few seconds ago"
      el.textContent = text
  return

# Refresh relative dates when the page changes
$.pageUpdate refreshContainerRelativeDates = ->
  refreshRelativeDates this

# Refresh relative dates every min
setInterval refreshRelativeDates, 60000
