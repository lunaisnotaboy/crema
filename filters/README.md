Filters
=======

Hooks that filter and rewrite markup on the client side. These run on DOM `ready` and any pjax load as part of the `pageUpdate` event. Useful when its too expensive or impossible to generate markup on the server side.

* MUST run on `pageUpdate`.
* MUST be implicitly initialized by the presense of `.js-*` class name or `data-*` attribute. No `$.fn.relatizeDate`.
* MUST be safe to run multiple times on the same element.
* Unlike behaviors, these SHOULD be expected to rewrite and insert HTML.
