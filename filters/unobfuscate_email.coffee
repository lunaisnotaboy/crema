# Unobtrusive Email Obfuscator
#
#= provides js-obfuscate-email
#
#= require jquery
#= require crema/events/pageupdate
#= require crema/utils/escapehtml
#
# Replaces `mail_to :encode => 'javascript'` which generates inline JS
# and uses eval. Yuck!
#
# Links must be annotated with a `js-obfuscate-email` class name and
# have a URI encoded address in `data-email`. See the
# `obfuscate_email` Rails helper. Any occurance of `"{email}"` will be
# replaced with the decoded email address.
#
# ### Markup
#
# ``` html
# <%= mail_to("{email}", "{email}",
#             :class => 'js-obfuscate-email',
#             'data-email' => obfuscate_email("chris@github.com") %>
# ```
#

$.pageUpdate rewriteObfuscatedEmails = ->
  for el in $(this).find '.js-obfuscate-email'
    $el = $ el

    # Skip if its already been ran once
    continue if $el.data 'obfuscate-email-setup'

    if obfuscatedEmail = $el.attr 'data-email'
      email = decodeURIComponent(obfuscatedEmail)

      # Replace {email} placeholder in contents
      inner = $el.text().replace /{email}/, email
      $el.html inner

      # Replace {email} placeholder href attribute
      if href = $el.attr 'href'
        # Strip any html tags
        cleanEmail = $.escapeHTML $('<div>').html(email).text()
        $el.attr 'href', href.replace /{email}/, cleanEmail

      # Mark as ran
      $el.data 'obfuscate-email-setup', true

  return
