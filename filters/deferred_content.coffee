# Deferred Content Loader
#
# Lazy loads uncached content.
#
#= provides js-deferred-content
#
#= require jquery
#= require crema/events/pageupdate
#= require crema/utils/ajax_poll
#
# Markup
#
#     <div class="js-deferred-content" data-url="/tree-commit">
#       Loading commit data...
#     </div>
#
# Will create an ajax poller request for `data-url`. If the response
# is a 202, the url will continue to be polled until a 200 or error is
# returned.
#
# When a 200 is received, the entire `js-deferred-content` container
# will be replaced via `$.fn.replaceWith`.
#
# IMPORTANT: The replaced content should no longer have a
#`js-deferred-content` or else you'll end up with an infinite request
# loop.
#
# If an error is received, an `error` class will be added to the
# container. This can be used for styling purposes.

loadContent = (el) ->
  $el = $ el
  return if $el.data 'deferred-content-loading'

  $.ajaxPoll
    url: $el.attr 'data-url'
    success: (data, status, xhr) ->
      # Skip on 202 and keep polling
      return if xhr.status is 202
      $data = $ $.parseHTML data
      $el.replaceWith $data
      $data.pageUpdate()
      $data.trigger 'deferredContent:loaded'
    error: ->
      $el.addClass 'error'

  $el.data 'deferred-content-loading', true
  return

$.pageUpdate loadDeferredContent = ->
  for el in $(this).find '.js-deferred-content'
    loadContent el
  return
