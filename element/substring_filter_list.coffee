# Element#substringFilterList
#
#= provides .substringFilterList
#
#= require jquery
#
# Filters list element items by a substring query
#
# ``` coffee
# $('ul').substringFilterList('gi')
# ```
#

# Filter list items by a search query.
#
# query - String search query
# options
#   text  - Function to extract item text (default: textContent)
#   limit - Number items limit
#
# Returns Number of visible items.
$.fn.substringFilterList = (query, options = {}) ->
  return unless list = this[0]

  query = query.toLowerCase()
  text  = options.text ? defaultText
  limit = options.limit
  items = $(list).children()

  if options.mark is true
    mark = defaultMarker
  else if options.mark?.call?
    mark = options.mark

  visible = 0

  for item in items
    if text(item).indexOf(query) isnt -1
      if limit and visible >= limit
        item.style.display = 'none'
      else
        visible++
        item.style.display = ''

        if mark
          mark item
          mark item, query
    else
      item.style.display = 'none'

  visible

# Get item text content.
#
# item - List item Element
#
# Returns String.
defaultText = (item) ->
  $.trim item.textContent.toLowerCase()

# Highlight text match.
#
# item - List item Element
# text - Text to highlight, if null remove highlights
#
# Returns nothing.
defaultMarker = (item, text) ->
  html = item.innerHTML
  if text
    re = new RegExp text, 'i'
    item.innerHTML = html.replace re, "<mark>$&</mark>"
  else
    clean = html.replace /<\/?mark>/g, ""
    item.innerHTML = clean if html isnt clean
  return
