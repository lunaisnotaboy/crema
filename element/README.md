Element
=======

Element `$.fn` extensions.

Usually a function that gets/sets or modifies the DOM element in some way. Best examples are under the jQuery [Attributes](http://api.jquery.com/category/attributes/) or [Manipulation](http://api.jquery.com/category/manipulation/) documention.

*(Not sure if collection filters would go here. Probably not, but I can't think of any I'd write that aren't already in jQuery.)*
