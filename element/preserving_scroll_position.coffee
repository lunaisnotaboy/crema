# Element#preservingScrollPosition
#
#= provides .preservingScrollPosition
#
#= require jquery
#= require crema/element/overflow_parent
#= require crema/element/positioned_offset
#= require crema/element/scrollto
#
# Executes a callback and ensures the previous scroll position is
# restored after.
#
# ``` coffeescript
# $('input').preservingScrollPosition ->
#   $('.comments').append newCommentHtml
# ```
#

# Preserves scroll position of element.
#
# callback - Function to call
#
# Returns chainable jQuery object.
$.fn.preservingScrollPosition = (callback) ->
  container = this.overflowParent()
  container = $(window) unless container[0].offsetParent?
  offset    = this.positionedOffset container

  top  = container.scrollTop()  - offset.top
  left = container.scrollLeft() - offset.left

  callback.call this

  offset = this.positionedOffset container
  container.scrollTo top: top + offset.top, left: left + offset.left

  this
