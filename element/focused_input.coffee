# Focused Input event handler helper
#
#= provides .onFocusedInput
#
#= require jquery
#
# Typically, delegating input events should be avoided if possible.
# An optimization is to delegate on focus instead and lazily bind the
# input handler.
#
# Instead of
#
# ``` coffeescript
# $(document).on 'input', '.foo-field', ->
#   check $(this).val()
# ```
#
# You'd do
#
# ``` coffeescript
# $(document).on 'focusin', '.foo-field', ->
#   $(this).on 'input', ->
#     check $(this).val()
#   $(this).on 'blur', ->
#     $(this).off 'input'
# ```
#
# To avoid the writting out the cleanup handlers, you can use this
# helper as shorthand.
#
# ``` coffeescript
# $(document).onFocusedInput '.foo-field', -> ->
#   check $(this).val()
# ```
#
# NOTE that the double `-> ->` is not a typo.
#
# If you want to do element caching on focus,
#
# ``` coffeescript
# $(document).onFocusedInput '.foo-field', ->
#   el = $(this).closest '.bar'
#   ->
#     check el, $(this).val()
# ```
#

# Install focus handler that lazily binds an input handler.
#
# selector     - String selector to delegate
# focusHandler - Function invoked on focus which may return
#                another handler for input events.
#
# Returns chained jQuery object.
$.fn.onFocusedInput = (selector, focusHandler) ->
  this.on 'focusin', selector, (focusEvent) ->
    ns = "focusInput#{Math.floor Math.random() * 1000}"

    if inputHandler = focusHandler.call this, focusEvent, ns
      $(this).on "input.#{ns}", inputHandler

    $(this).on "blur.#{ns}", ->
      $(this).off ".#{ns}"
      return

    return

  this
