# Element#fire
#
#= provides .fire
#
#= require jquery
#
# Initializes and dispatches event from target.
#
# Nicer API to replace jQuery's builtin `$.fn.trigger`.
#
# ### Options
#
# * **type** String event name.
# * **props** Object of properties to extend the event with (default: `{cancelable: false, bubbles: true}`).
# * **args** - Array of extra arguments to invoke handlers with (default: `[]`).
# * **defaultAction** Function to invoke unless `preventDefault()` is called.
#
# ``` coffeescript
# $('#menu').fire 'menu:closed'
# $('#menu').fire 'menu:over', bubbles: false
# $('#menu').fire 'menu:activate', -> $(this).show()
# $('.item').fire 'item:selected', [selection]
# ```
#

# Initializes and triggers event from target.
#
# Works the same as $.fn.trigger, but has nicer arguments and options.
#
# type          - String event name.
# props         - Object of properties to extend event with.
# args          - Array of extra arguments to invoke handlers with.
# defaultAction - Function to invoke unless preventDefault() is
#                 called.
#
# Returns jQuery.Event if fired synchronously and undefined if fired
# asynchronously.
$.fn.fire = (type) ->
  if arg = arguments[1]
    if $.isPlainObject arg
      props = arg
    else if $.isArray arg
      args = arg
    else if $.isFunction arg
      defaultAction = arg

  if arg = arguments[2]
    if $.isArray arg
      args = arg
    else if $.isFunction arg
      defaultAction = arg

  if arg = arguments[3]
    if $.isFunction arg
      defaultAction = arg

  element = this[0]
  props ?= {}
  props.cancelable ?= !!defaultAction
  props.bubbles ?= true
  args ?= []

  trigger = ->
    event = $.Event type, props
    $.event.trigger event, args, element, !event.bubbles

    if defaultAction and not event.isDefaultPrevented()
      defaultAction.call element, event

    event

  if props.async
    delete props.async
    setTimeout trigger, 0
    return
  else
    trigger()
