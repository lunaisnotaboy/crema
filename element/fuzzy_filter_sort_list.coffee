# Element#fuzzyFilterSortList
#
#= provides .fuzzyFilterSortList
#
#= require jquery
#= require crema/utils/fuzzy
#
# Filters and sorts list element items by a fuzzy query
#
# ``` coffee
# $('ul').fuzzyFilterSortList('gi')
# ```
#

# Filter list items by a search query.
#
# query - String search query
# options
#   content - Function to extract item content (default: item)
#   text    - Function to extract item text (default: textContent)
#   limit   - Number items limit
#
# Returns Number of visible items.
$.fn.fuzzyFilterSortList = (query, options = {}) ->
  return unless list = this[0]

  query   = query.toLowerCase()
  content = options.content ? defaultContent
  text    = options.text ? defaultText
  limit   = options.limit

  if options.mark is true
    mark = defaultMarker
  else if options.mark?.call?
    mark = options.mark

  if allItems = $(list).data 'fuzzy-sort-items'
    children = $(list).children()
  else
    children = allItems = $(list).children()
    $(list).data 'fuzzy-sort-items', allItems.slice(0)

  for item in children
    list.removeChild item
    item.style.display = ''

  results = document.createDocumentFragment()
  total = 0
  visible = 0

  if !query
    for item in allItems
      if !limit or total < limit
        visible++
        mark content(item) if mark
        results.appendChild item
      total++

  else
    items = allItems.slice 0

    for item in items
      item.fuzzyFilterTextCache ?= text(content(item))
      score = $.fuzzyScore item.fuzzyFilterTextCache, query
      item.fuzzyFilterScoreCache = score

    # Sort by sort, then alpha
    items.sort sortByScore

    # Build regexp that matches any chars from the query
    queryRe = $.fuzzyRegexp query

    for item in items
      if (!limit or total < limit) and item.fuzzyFilterScoreCache > 0
        visible++
        if mark
          c = content item
          mark c
          mark c, query, queryRe
        results.appendChild item
      total++

  list.appendChild results
  visible

# Sort elements by fuzzy score and alpha.
#
# a - First Element
# b - Second Element
#
# Returns -1, 0, 1.
sortByScore = (a, b) ->
  as = a.fuzzyFilterScoreCache
  bs = b.fuzzyFilterScoreCache
  at = a.fuzzyFilterTextCache
  bt = b.fuzzyFilterTextCache
  if as > bs then -1
  else if as < bs then 1
  else if at < bt then -1
  else if at > bt then 1
  else 0

# Get content element for item.
#
# Used for extract text and highlighting.
#
# item - List item Element.
#
# Returns Element.
defaultContent = (item) ->
  item

# Get item text content.
#
# content - List item content Element
#
# Returns String.
defaultText = (content) ->
  $.trim content.textContent.toLowerCase()

# Use fuzzy highlight for default marker
defaultMarker = $.fuzzyHighlight
