# Element#hasFocus
#
#= provides .hasFocus
#
#= require jquery
#
# Check if container has any fields that have active focus.
#
# ``` coffeescript
# $('textarea').hasFocus() # => true
# ```
#

# Check if container has any fields that have active focus.
#
# Returns true if container has any focused fields.
$.fn.hasFocus = ->
  return false unless container = this[0]
  contained = document.activeElement
  container is contained or $.contains container, contained
