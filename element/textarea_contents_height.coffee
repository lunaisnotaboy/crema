# Element#textareaContentsHeight
#
#= provides .textareaContentsHeight
#
#= require jquery
#= require crema/element/textarea_mirror
#
# Get height of the text contents of a `textarea` field.
#
# ``` coffeescript
# $('textarea').textareaContentsHeight()
# #=> 100
# ```
#

# Measures the height of the contents in textarea.
#
# Returns Number of pixels.
$.fn.textareaContentsHeight = ->
  return unless textarea = this[0]
  return unless mirror = $.fn.textareaMirror.call [textarea], false
  mirror.style.height = ""
  height = $.css mirror, 'height', 'content'
  setTimeout ->
    mirror.parentNode?.removeChild mirror
  , 5000
  height
