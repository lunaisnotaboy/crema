# Element#touch
#
#= provides .touch
#
#= require jquery
#
# Force relayout for element.
#
# This is intended to working around browser rendering bugs. Only use
# this if you know you must.
#
# ``` coffeescript
# # Resets className so a selector recheck is scheduled
# $(div).touch()
#
# # Force immediate relayout by accessing an offset property
# $(div).touch(true)
# ```
#

# Force relayout.
#
# immediate - Force synchronous relayout (default: false)
#
# Returns chainable jQuery object.
$.fn.touch = (immediate) ->
  if immediate
    el.offsetHeight for el in this
  else
    el.className = el.className for el in this
  this
