# Element#overflowParent
#
#= provides .overflowParent
#
#= require jquery
#
# Get the closet ancestor element that has a scroll overflow.
#
# ``` coffeescript
# $('.site').overflowParent() # => $('body')
# $('.item').overflowParent() # => $('.results')
# ```
#

# Will walk up the DOM from the element until it reaches an element
# with overflow scroll. Basically, an element that can have scroll bars.
#
# Returns a jQuery wrapped Element.
$.fn.overflowParent = ->
  return this unless element = this[0]
  until element is document.body
    overflowY = $(element).css 'overflow-y'
    overflowX = $(element).css 'overflow-x'
    if overflowY is 'auto' or overflowX is 'auto' or
        overflowY is 'scroll' or overflowX is 'scroll'
      break
    element = element.parentElement
  $ element
