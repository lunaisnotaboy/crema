# Element#hasMousedown
#
#= provides .hasMousedown
#
#= require jquery
#
# Check if mouse is down on an element
#
# ``` coffeescript
# $('.link').hasMousedown() # => true
# ```
#

# Internal: Track last mousedown event.
lastMouseDownEvent = null

# Record last mousedown event.
document.addEventListener 'mousedown', (event) ->
  lastMouseDownEvent = event
  return
, true

# Clear mousedown event on release.
document.addEventListener 'mouseup', (event) ->
  lastMouseDownEvent = null
  return
, true

# Check if mouse is pressed down inside a container.
#
# Returns true if mouse is pressed down on an element inside the
# container, otherwise false.
$.fn.hasMousedown = ->
  return false unless container = this[0]

  if lastMouseDownEvent
    contained = lastMouseDownEvent.target
    container is contained or $.contains container, contained
  else
    false
