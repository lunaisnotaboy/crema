# Element#eventPropagationPath
#
#= provides .eventPropagationPath
#
#= require jquery
#
# Returns DOM 3 Events event propagation path for an Element.
#
# ``` coffeescript
# $('body').eventPropagationPath()
# # => ['body', 'html', document, window]
# ```
#

# Determine bubble propagation path.
#
#
# Returns an Array of Nodes.
$.fn.eventPropagationPath = ->
  return [] unless node = this[0]

  # window
  if $.isWindow node
    [node]

  # document
  else if node.nodeType is 9
    [node, node.defaultView]

  # element
  else if node.nodeType is 1
    parents  = $.makeArray $(node).parents()
    document = node.ownerDocument
    window   = document.defaultView
    [node].concat(parents).concat([document, window])
