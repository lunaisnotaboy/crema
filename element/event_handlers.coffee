# Element#eventHandlers
#
#= provides .eventHandlers
#
#= require jquery
#= require crema/element/event_propagation_path
#
# Returns all jQuery event handlers for an Element.
#
# ``` coffeescript
# $('.foo').eventHandlers().click # => [...]
# ```
#

# Get all matching event handlers for element.
#
# Returns an Object of type keys and Array values.
$.fn.eventHandlers = (type) ->
  return unless target = this[0]

  handlers = {}

  for el in $(target).eventPropagationPath()
    dispatchPath = []
    current = target
    while current isnt el
      dispatchPath.push current
      current = current.parentNode ? el

    # TODO: This uses a private jQuery API
    for type, handlerObjs of $._data(el, 'events')
      for handlerObj in handlerObjs
        if sel = handlerObj.selector
          for current in dispatchPath
            if $(current).is(sel)
              handlers[type] ?= []
              handlers[type].push handlerObj

        else
          handlers[type] ?= []
          handlers[type].push handlerObj

  handlers
