# Element#overflowOffset
#
#= provides .overflowOffset
#
#= require jquery
#= require crema/element/positioned_offset
#
# Returns overflow/scroll offset relative to the container.
#
# This value is useful for figuring out if the element is outside the
# scrollbars of the container. If `top` is negative, the top of the
# element is above the scroll view. If `bottom` is negative, the
# bottom of the element is below the scroll view.
#
#     +--------------------+
#     | (outside viewport) |
#     +--------------------+ -|
#     |          |         |  |
#     |    top > |         |  |
#     |          |         |  | h
#     |          |         |  | e
#     |  v left  V right v |  | i
#     |-----> Element <----|  | g
#     |          ^         |  | h
#     |          |         |  | t
#     | bottom > |         |  |
#     |          |         |  |
#     +--------------------+ -|
#     | (outside viewport) |
#     +--------------------+
#
# ``` coffeescript
# $('div').overflowOffset()
# #=> {top: 100, left: 100, bottom: 800, right: 800,
#      height: 1000, width 1000}
# ```
#

# Get element overflow offset.
#
# container - Scrollable Element container (defaults: document.body)
#
# Return an Object with `top`, `left`, `bottom`, `right`, `height`,
# and `width` properties.
$.fn.overflowOffset = (container = document.body) ->
  return null unless element = this[0]

  offset = $(element).positionedOffset container

  if container.offsetParent
    scroll = top: $(container).scrollTop(), left: $(container).scrollLeft()
  else
    scroll = top: $(window).scrollTop(), left: $(window).scrollLeft()
    container = document.documentElement

  top  = offset.top  - scroll.top
  left = offset.left - scroll.left

  height = container.clientHeight
  width  = container.clientWidth

  bottom = height - (top  + element.offsetHeight)
  right  = width  - (left + element.offsetWidth)

  {top, left, bottom, right, height, width}
