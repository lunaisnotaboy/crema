# Element#scrollTo
#
#= provides .scrollTo
#
#= require jquery
#= require crema/element/positioned_offset
#
# Scrolls element inside container.
#
# ``` coffeescript
# $('#new-comment').scrollTo()
#
# # Scroll container to target
# $('#results').scrollTo result
#
# # Smooth scroll
# $('#new-comment').scrollTo duration: 200
# ```
#

# Scroll to element inside container.
#
# Returns chainable jQuery object.
$.fn.scrollTo = (args...) ->
  return this unless container = this[0]

  options = {}

  if $.isPlainObject args[0]
    options = args[0]
    options.complete ?= args[1] if $.isFunction args[1]

  else if args[0]?
    options.target = args[0]

  if !options.top? and !options.left?
    if options.target
      {top, left} = $(options.target).positionedOffset container
      options.top  = top
      options.left = left
    else
      {top, left} = $(container).positionedOffset()
      options.top  = top
      options.left = left
      container    = document

  if !container.offsetParent
    if options.duration
      # Animating both html and body is shitty
      # body works in Webkit, html works in FF and IE
      animateScroll 'html, body', options
    else
      $(document).scrollTop options.top   if options.top?
      $(document).scrollLeft options.left if options.left?
      options.complete?()

  else
    if options.duration
      animateScroll container, options
    else
      container.scrollTop  = options.top  if options.top?
      container.scrollLeft = options.left if options.left?
      options.complete?()

  this

# Smooth scrollin
#
# container - Element to animate scroll offsets on
# options   - options object from scrollTo
#
# Returns nothing.
animateScroll = (container, options) ->
  props = {}
  props.scrollTop  = options.top  if options.top?
  props.scrollLeft = options.left if options.left?

  opts = duration: options.duration, queue: false

  if options.complete
    count = $(container).length
    opts.complete = ->
      setTimeout(options.complete, 0) if --count is 0

  $(container).animate(props, opts)
