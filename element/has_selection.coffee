# Element#hasSelection
#
#= provides .hasSelection
#
#= require jquery
#
# Check if container has a user selection.
#
# ``` coffeescript
# $('.markdown-body').hasSelection() # => true
# ```
#

# Check if container has a user selection.
#
# Returns true if container has user selected text, otherwise false.
$.fn.hasSelection = ->
  return false unless container = this[0]

  selection = window.getSelection()
  if selection and selection.type is 'Range' and selection.focusNode?
    contained = selection.focusNode
    container is contained or $.contains container, contained
  else
    false
