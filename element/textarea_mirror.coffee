# Element#textareaMirror
#
#= provides .textareaMirror
#
#= require jquery
#
# Create mirrored div of textarea for measurements
#
# ``` coffeescript
# mirror = $('textarea').textareaMirror().
# $(mirror).scrollHeight()
# ```

# Static CSS properties to set on hidden mirror element.
properties = [
  'position:absolute;',
  'overflow:auto;',
  'white-space:pre-wrap;',
  'word-wrap:break-word;',
  'top:0px;',
  'left:-9999px;'
]

# Copy CSS properties from textarea to div that would
# affect the cusor position.
propertyNamesToCopy = [
  'box-sizing'
  'font-family'
  'font-size'
  'font-style'
  'font-variant'
  'font-weight'
  'height'
  'letter-spacing'
  'line-height'
  'padding-bottom'
  'padding-left'
  'padding-right'
  'padding-top'
  'text-decoration'
  'text-indent'
  'text-transform'
  'width'
  'word-spacing'
]

# Builds offscreen div that mirrors the textarea.
#
# markerPosition - Optional Number to position a cursor marker at
#                  (defaults to the end of the text)
#
# Returns an Element attached to the DOM. It is the callers
# responsiblity to cleanup and remove the element after they are
# finished with their measurements.
$.fn.textareaMirror = (markerPosition) ->
  return unless textarea = this[0]

  # Must be a TextAreaElement
  return unless textarea.nodeName.toLowerCase() is 'textarea'

  # See if a mirror element is already initialized
  mirror = textarea.previousElementSibling

  if mirror and mirror.className is 'js-textarea-mirror'
    mirror.innerHTML = ""
  else
    mirror = document.createElement 'div'
    mirror.className = 'js-textarea-mirror'

    style = window.getComputedStyle textarea
    props = properties.slice 0
    for name in propertyNamesToCopy
      props.push "#{name}:#{style[name]};"
    mirror.style.cssText = props.join ' '

  if markerPosition isnt false
    marker = document.createElement 'span'
    marker.className = 'js-marker'
    marker.innerHTML = "&nbsp;"

  if typeof markerPosition is 'number'
    if text = textarea.value.substring 0, markerPosition
      before = document.createTextNode text

    if text = textarea.value.substring markerPosition
      after = document.createTextNode text
  else
    if text = textarea.value
      before = document.createTextNode text

  mirror.appendChild before if before
  mirror.appendChild marker if marker
  mirror.appendChild after  if after

  # Insert mirror into the dom if it isn't already
  unless mirror.parentElement
    textarea.parentElement.insertBefore mirror, textarea

  mirror.scrollTop = textarea.scrollTop

  mirror
