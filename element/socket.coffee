# XHR Socket
#
#= provides .socket
#
#= require jquery
#= require crema/utils/socket
#
# ### Markup
#
#     <link rel="xhr-socket" href="/_socket">
#

# Skip unless browser supports socket
return unless $.socket

# Get current socket instance or initialize a new socket if one does not
# exist or its closed.
#
# Returns XhrSocket instance.
$.fn.socket = ->
  # Return if set is empty
  return unless link = this[0]

  # Skip unless element is a link[rel=xhr-socket]
  return unless $(link).is 'link[rel=xhr-socket]'

  # Grab existing socket if one exists
  socket = $(link).data 'socket'

  # If the existing one is already active, return it
  if socket and socket.readyState isnt socket.CLOSED
    return socket

  socket = $.socket link.href

  socket.on 'open',   -> $(link).trigger 'socket:open', [this]
  socket.on 'close',  -> $(link).trigger 'socket:close', [this]
  socket.on 'reopen', -> $(link).trigger 'socket:reopen', [this]

  socket.on 'send',    (data) -> $(link).trigger 'socket:send', [data, this]
  socket.on 'message', (data) -> $(link).trigger 'socket:message', [data, this]

  $(link).data 'socket', socket
  socket
