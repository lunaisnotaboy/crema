# Element#positionedOffset
#
#= provides .positionedOffset
#
#= require jquery
#
# Returns position offset relative to the container.
#
# Measures the number of pixels from the top of the container to the
# top of the element and the number of pixels from the bottom of the
# containers full scroll height to the bottom of the element. If
# container is `body`, this is the same as $(element).offset().
#
#     +---------------------+ -|
#     |  outside | viewport |  |
#     +----------|----------+  |
#     |          |          |  |
#     |    top > |          |  |
#     |          |          |  | h
#     |          |          |  | e
#     |  v left  V right v  |  | i
#     |-----> Element <-----|  | g
#     |          ^          |  | h
#     |          |          |  | t
#     | bottom > |          |  |
#     |          |          |  |
#     +----------|----------+  |
#     |  outside | viewport |  |
#     +---------------------+ -|
#
# ``` coffeescript
# $('div').positionedOffset()
# #=> {top: 100, left: 100, bottom: 800, right: 800}
# ```
#

# Get element positioned offset.
#
# This value is useful for assigning to `scrollTop` to scroll to the item.
#
# container - Scrollable Element container. Defaults to 'document'.
#
# Returns an Object with `top`, 'left', `bottom`, and `right` properties.
$.fn.positionedOffset = (container) ->
  return unless element = this[0]

  # Allow for wrapped jQuery objects
  container = container[0] if container?.jquery

  top  = 0
  left = 0

  height = element.offsetHeight
  width  = element.offsetWidth

  until !element or element is container
    top    += element.offsetTop or 0
    left   += element.offsetLeft or 0
    element = element.offsetParent

  if !container or !container.offsetParent
    scrollHeight = $(document).height()
    scrollWidth  = $(document).width()
  else
    scrollHeight = container.scrollHeight
    scrollWidth  = container.scrollWidth

  bottom = scrollHeight - (top + height)
  right  = scrollWidth  - (left + width)

  {top, left, bottom, right}

isRootNode = (node) ->
  $.isWindow(node) or node.nodeType is 9 or node.nodeName?.match(/body|html/i)
