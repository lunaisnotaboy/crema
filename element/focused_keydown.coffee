# Focused Keydown event handler helper
#
#= provides .onFocusedKeydown
#
#= require jquery
#
# Typically, delegating keydown events should be avoided if possible.
# An optimization is to delegate on focus instead and lazily bind the
# keydown handler.
#
# See sibling .onFocusedInput API.
#

# Install focus handler that lazily binds an keydown handler.
#
# selector     - String selector to delegate
# focusHandler - Function invoked on focus which may return
#                another handler for keydown events.
#
# Returns chained jQuery object.
$.fn.onFocusedKeydown = (selector, focusHandler) ->
  this.on 'focusin', selector, (focusEvent) ->
    ns = "focusKeydown#{Math.floor Math.random() * 1000}"

    if keydownHandler = focusHandler.call this, focusEvent, ns
      $(this).on "keydown.#{ns}", keydownHandler

    $(this).on "blur.#{ns}", ->
      $(this).off ".#{ns}"
      return

    return

  this
