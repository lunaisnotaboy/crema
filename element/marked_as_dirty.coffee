# Element#markedAsDirty
#
#= provides .markedAsDirty
#
#= require jquery
#
# Check if any element in the container is explicitly marked as
# dirty by a `js-dirty` class name.
#
# ``` coffeescript
# $('form').markedAsDirty() # => false
# ```
#

# Check if any element in the container is explicitly marked as
# dirty by a `js-dirty` class name.
#
# Returns true if container is dirty, otherwise false.
$.fn.markedAsDirty = ->
  this.find('.js-dirty').length > 0
