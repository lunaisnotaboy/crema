# Element#hasDirtyFields
#
#= provides .hasDirtyFields
#
#= require jquery
#
# Check if container has any input or textarea fields that have been
# modified by the user.
#
# ``` coffeescript
# $('form').hasDirtyFields() # => false
# ```
#

# Check if container has any input or textarea fields that have been
# modified by the user.
#
# Returns true if any field's value has changed from its default
# value, otherwise false.
$.fn.hasDirtyFields = ->
  for field in this.find 'input, textarea'
    if field.value isnt field.defaultValue
      return true
  false
