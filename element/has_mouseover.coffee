# Element#hasMouseover
#
#= provides .hasMouseover
#
#= require jquery
#
# Check if mouse is over an element
#
# ``` coffeescript
# $('.foo').hasMouseover() # => true
# ```
#

# Internal: Track last mousemove event.
lastMouseMoveEvent = null

# Record last mousemove event
document.addEventListener 'mousemove', (event) ->
  lastMouseMoveEvent = event
  return
, true

# Check if mouse is over a container.
#
# Returns true if mouse is over on an element inside the
# container, otherwise false.
$.fn.hasMouseover = ->
  return false unless container = this[0]

  if lastMouseMoveEvent
    contained = lastMouseMoveEvent.target
    container is contained or $.contains container, contained
  else
    false
