# Element#performTransition
#
#= provides .performTransition
#
#= require jquery
#= require sprockets-modernizr
#
# CSS3 transitions have some major pitfalls. First trying to
# transition elements from display:none to display:block. And also
# dealing with computed properties like height:auto.
#
# This helper attempts to deal with those issues while still defining
# the transitions in pure css.
#
# Behaviors that intend to support these special case transitions
# should call `performTransition` when they add or remove their state
# classes.
#
# Any element that is performing a transition needs to be annotated
# with the `js-transitionable` class name.
#
# Example
#
# ``` html
# <div class="js-transitionable collapse">
#   Slide down
# </div>
# ```
#
# ``` css
# .collapse {
#   display: none;
#   position: relative;
#   height: 0;
#   overflow: hidden;
#   @include transition(0.35s, height, ease);
# }
# .collapse.open {
#   display: block;
#   height: auto;
# }
# ```
#
# ``` coffeescript
# $('.collapse').performTransition ->
#   this.addClass 'open'
# ```
#

$.fn.performTransition = (cb) ->
  # If browser doesn't support transitions, skip and just invoke the
  # callback
  if !transitionEndEventName
    cb.apply this
    return

  # Gather all element in the container annotated with
  # js-transitionable
  els = this.find '.js-transitionable'
  els = els.add this.filter '.js-transitionable'

  for el in els
    $el = $ el

    # Detect if this is going to be a height transition, we need to
    # do some special prep
    transitionHeight = isTransitioningHeight el

    $el.one transitionEndEventName, ->
      # Reset any visibility changes from start of transition
      el.style.display    = null
      el.style.visibility = null

      # Reset height to whatever if was if we changed it
      if transitionHeight
        withoutTransition el, ->
          el.style.height = null

    # Ensure element is visible anytime we are transitioning
    el.style.display    = 'block'
    el.style.visibility = 'visible'

    # realize height:auto property
    if transitionHeight
      withoutTransition el, ->
        el.style.height = "#{$el.height()}px"

    # Force reflow so our inline style changes are applied before any
    # transition starts
    el.offsetHeight

  # Apply callback to kick off transition
  cb.apply this

  for el in els
    # If this is a height transition, toggle property
    if isTransitioningHeight el
      # If 0, expand element to full scroll height
      if $(el).height() is 0
        el.style.height = "#{el.scrollHeight}px"
      else
        # Else, collapse to 0
        el.style.height = "0px"

  this


transitionEndEventNames =
  'WebkitTransition': 'webkitTransitionEnd'
  'MozTransition':    'transitionend'
  'OTransition':      'oTransitionEnd'
  'msTransition':     'MSTransitionEnd'
  'transition':       'transitionend'

# Get prefixed transitionEnd event name
# Will be undefined if the browser doesn't support transitions
transitionEndEventName = transitionEndEventNames[Modernizr.prefixed 'transition']

# Detect if element is transitioning its height property.
#
# el - The Element
#
# Returns true or false.
isTransitioningHeight = (el) ->
  name = Modernizr.prefixed 'transitionProperty'
  $(el).css(name) is 'height'

# Apply style change callback w/o triggering a transition
#
# el - Element that will be modified
# cb - Callback to make style changes in
#
# Returns nothing.
withoutTransition = (el, cb) ->
  el.style.webkitTransition = 'none'
  cb el
  # Trigger reflow so styles are rendered w/o transition
  el.offsetHeight
  el.style.webkitTransition = null
  return
