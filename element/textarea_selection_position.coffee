# Element#textareaSelectionPosition
#
#= provides .textareaSelectionPosition
#= require crema/element/textarea_mirror
#
#= require jquery
#
# Get offset position of cursor in a `textarea` field. The offset is the
# number of pixels from the top left of the `textarea`. Useful for
# positioning a popup near the insertion point.
#
# ``` coffeescript
# {top, left} = $('textarea').textareaSelectionPosition()
# ```
#

# Measures offset position of cursor in textarea.
#
# Returns object with {top, left} properties.
$.fn.textareaSelectionPosition = ->
  return unless textarea = this[0]

  # Must support selectionEnd. Older versions of IE don't.
  return unless textarea.selectionEnd?

  return unless mirror = $(textarea).textareaMirror(textarea.selectionEnd)
  position = $(mirror).find('.js-marker').position()
  setTimeout ->
    $(mirror).remove()
  , 5000
  position
