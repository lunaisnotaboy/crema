# Group logs by page URL
#
#= require jquery
#= require crema/events/pagechange
#= require crema/events/pageupdate
#= require crema/detects/consolecssformatter

# Skip unless browser supports new console apis
return unless console.group?

if performance?
  now = -> performance.now()
else
  now = -> Date.now()

logPageEvent = (type, el) ->
  time = Math.ceil now() - startTimestamp

  if Modernizr.consolecssformatter
    console.groupCollapsed "%c%s %c(%sms)",
      "font-weight:normal", type,
      "font-weight:normal;color:#666;", time
  else
    console.groupCollapsed "%s (%sms)", type, time

  console.timeStamp type
  console.log el
  console.trace()
  console.groupEnd()

currentPath = null
startTimestamp = null
markPageGroup = ->
  startTimestamp = now()
  return if currentPath is window.location.pathname
  currentPath = window.location.pathname
  console.groupEnd()
  console.group currentPath


$.pageUpdate.before = (el) -> markPageGroup()
$.pageUpdate.after  = (el) -> logPageEvent 'pageUpdate', el

$.pageChange.before = (el) -> markPageGroup()
$.pageChange.after  = (el) -> logPageEvent 'pageChange', el

markPageGroup()
