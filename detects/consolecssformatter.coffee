# console.log css formatter support
#
#= require sprockets-modernizr
#
# https://developers.google.com/chrome-developer-tools/docs/console#styling_console_output_with_css

# Too difficult to feature detect, Safari should soon.
Modernizr.addTest 'consolecssformatter', chrome?
