# Navigation Behavior
#
#= provides js-navigation-container
#= provides js-navigation-item
#
#= require jquery
#= require crema/element/fire
#= require crema/element/overflow_offset
#= require crema/element/overflow_parent
#= require crema/element/positioned_offset
#= require crema/element/scrollto
#= require crema/events/hotkey
#= require crema/events/pageupdate
#
# Provides basic cursor, vi and emacs keyboard navigation. As well as
# mouseover selection.
#
# * **item** Element with `.js-navigation-item`. The current focused
#            item is denoted with the "navigation-focus" class name.
# * **container** Element with `.js-navigation-container` class name.
#                 Contains item elements. The active container is denoted
#                 by the `.js-active-navigation-container` class.
#
# The `.js-active-navigation-container` class can be part of the html or
# added programmatically with `navigation('focus')`. Making it part of
# the template will automatically activate it without any extra JS.
# Just make sure its the only active container on the page.
#
# ``` css
# li.navigation-focus { background: yellow; }
# ```
# ``` html
# <ul class="js-navigation-container js-active-navigation-container">
#   <li class="js-navigation-item"><a href="">One</a></li>
#   <li class="js-navigation-item"><a href="">Two</a></li>
#   <li class="js-navigation-item"><a href="">Three</a></li>
# </ul>
# ```
#
# ### Methods
#
# #### `.navigation('active')`
#
# Returns current active container.
#
# #### `.navigation('activate')`
#
# Activates navigation container.
#
# #### `.navigation('deactivate')`
#
# Deactivates navigation container.
#
# #### `.navigation('push')`
#
# Activates navigation container and push it onto a stack.
#
# #### `.navigation('pop')`
#
# Deactivates navigation container and remove it from stack. Then
# activate the previous container on the top of the stack.
#
# #### `.navigation('focus')`
#
# Activates navigation container and focus first item.
#
# #### `.navigation('clear')`
#
# Removes current focus.
#
# #### `.navigation('refocus')`
#
# Removes current focus and focuses on the first item.
#
#
# ### Events
#
# `navigation:activate`
#
# * **Bubbles** Yes
# * **Cancelable** Yes
# * **Default action** Activate navigation
# * **Target** `.js-navigation-container` Element
#
# `navigation:activated`
#
# * **Bubbles** Yes
# * **Cancelable** No
# * **Target** `.js-navigation-container` Element
#
# `navigation:deactivate`
#
# * **Bubbles** Yes
# * **Cancelable** Yes
# * **Default action** Deactivate navigation
# * **Target** `.js-navigation-container` Element
#
# `navigation:deactivated`
#
# * **Bubbles** Yes
# * **Cancelable** No
# * **Target** `.js-navigation-container` Element
#
# `navigation:focus`
#
# * **Bubbles** Yes
# * **Cancelable** Yes
# * **Default action** Focus navigation item
# * **Target** `.js-navigation-item` Element
#
# `navigation:focused`
#
# * **Bubbles** Yes
# * **Cancelable** No
# * **Target** `.js-navigation-item` Element
#
# `navigation:keydown`
#
# * **Bubbles** Yes
# * **Cancelable** Yes
# * **Default action** Deactivate navigation
# * **Target** `.js-navigation-item` or `.js-navigation-container` Element
# * **Context info**
#   * `originalEvent` - `keydown` event
#   * `hotkey`        - Original event hotkey property
#   * `relatedTarget` - `.js-navigation-container` Element
#
# `navigation:keyopen`
#
# * **Bubbles** Yes
# * **Cancelable** No
# * **Target** `.js-navigation-item` Element
# * **Context info**
#   * `modifierKey` - Boolean for ctrl or command modifier key
#
# `navigation:open`
#
# * **Bubbles** Yes
# * **Cancelable** Yes
# * **Target** `.js-navigation-item` Element
# * **Context info**
#   * `modifierKey` - Boolean for ctrl or command modifier key
#

ctrlBindings = navigator.userAgent.match /Macintosh/
modifierKey = if navigator.userAgent.match /Macintosh/ then 'meta' else 'ctrl'

disableMouseEvents = false
mousePosition = x: 0, y: 0

# Rebinds mouseover handlers on navigation containers.
#
# This is an optimization to avoid adding a global mouseover to
# document.
$.pageUpdate installNavigationMouseListener = ->
  for container in $ '.js-navigation-container'
    $container = $ container
    continue if $container.data 'navigation-installed'

    # Allow mouse observers to be disabled
    if $container.attr('data-navigation-disable-mouse') is undefined
      $container.on 'mousemove', onContainerMouseMove
      $container.on 'mouseover', onContainerMouseOver

    $container.data 'navigation-installed', true
  return

# Tracks last mouse position inside container.
#
# Returns nothing.
onContainerMouseMove = (event) ->
  # If mouse position moved, re-enable mouseover handler
  if mousePosition.x isnt event.clientX or mousePosition.y isnt event.clientY
    disableMouseEvents = false
  mousePosition = x: event.clientX, y: event.clientY
  return

# Triggers internal `navigation:mouseover` event.
#
# Event is handled by `onItemMouseOver`.
#
# Returns nothing.
onContainerMouseOver = (event) ->
  return if disableMouseEvents
  $(event.target).trigger 'navigation:mouseover'
  return

# mouseOverObserverInstalled = false

# Dispatches custom events from focused items.
#
# It also makes it easy to bind custom hotkeys to focused items.
#
# Examples
#
#     $(document).on 'navigation:keydown', '.listings .listing', (event) ->
#       if event.hotkey is 'x'
#         $(this).click()
#         false
#
# Returns Boolean.
$(document).on 'keydown', (event) ->
  # Navigation must be explicitly enabled on text inputs
  if event.target isnt document.body and !$(event.target).hasClass('js-navigation-enable')
    return

  # Ignore any key press if no container is focused
  return unless container = getActiveContainer()

  disableMouseEvents = true

  target = $(container).find('.js-navigation-item.navigation-focus')[0] or container

  customEvent = $(target).fire 'navigation:keydown',
    originalEvent: event
    hotkey: event.hotkey
    relatedTarget: container

  # Cancel original keydown event if custom navigation:keydown is canceled
  if customEvent.isDefaultPrevented()
    event.preventDefault()
  customEvent.result

# Handle keydown events.
#
# event - jquery.Event navigation:keydown event
#
# Returns nothing.
$(document).on 'navigation:keydown', '.js-active-navigation-container', (event) ->
  container = this

  # If target is a input, only handle modeless bindings so people
  # can still type 'j' and 'k'.
  isInput = $(event.originalEvent.target).is ':input'

  # If an item has focus, handle moving to next and previous items
  if $(event.target).is '.js-navigation-item'
    item = event.target

    if isInput
      if ctrlBindings
        switch event.hotkey
          # Emacs
          when 'ctrl+n' then cursorDown item, container
          when 'ctrl+p' then cursorUp item, container

      switch event.hotkey
        # Arrows
        when 'up'   then cursorUp item, container
        when 'down' then cursorDown item, container

        # Open
        when 'enter' then keyOpen item
        when "#{modifierKey}+enter" then keyOpen item, true

    else
      if ctrlBindings
        switch event.hotkey
          # Emacs
          when 'ctrl+n' then cursorDown item, container
          when 'ctrl+p' then cursorUp item, container
          when 'alt+v'  then pageUp item, container
          when 'ctrl+v' then pageDown item, container

      switch event.hotkey
        # Vim
        when 'j' then cursorDown item, container
        when 'k' then cursorUp item, container

        # Open
        when 'o', 'enter' then keyOpen item
        when "#{modifierKey}+enter" then keyOpen item, true

  # If no item is selected, focus the first item
  else
    item = getItems(container)[0]

    if isInput
      if ctrlBindings
        switch event.hotkey
          # Emacs
          when 'ctrl+n' then focusItem item, container

      switch event.hotkey
        # Arrows
        when 'down' then focusItem item, container

    else
      if ctrlBindings
        switch event.hotkey
          # Emacs
          when 'ctrl+n', 'ctrl+v' then focusItem item, container

      switch event.hotkey
        # Vim
        when 'j' then focusItem item, container

  # Disable default action for all possible key combos that are handled
  if isInput
    if ctrlBindings
      switch event.hotkey
        # Emacs
        when 'ctrl+n', 'ctrl+p' then event.preventDefault()

    switch event.hotkey
      # Arrows
      when 'up', 'down' then event.preventDefault()
      # Open
      when 'enter', "#{modifierKey}+enter" then event.preventDefault()

  else
    if ctrlBindings
      switch event.hotkey
        # Emacs
        when 'ctrl+n', 'ctrl+p', 'alt+v', 'ctrl+v' then event.preventDefault()

    switch event.hotkey
      # Vim
      when 'j', 'k' then event.preventDefault()
      # Open
      when 'o', 'enter', "#{modifierKey}+enter" then event.preventDefault()

  return

# Public: Focus the item on mouseover.
#
# event - jquery.Event triggered from item
#
# Returns nothing.
$(document).on 'navigation:mouseover', '.js-active-navigation-container .js-navigation-item', (event) ->
  container = $(event.currentTarget).closest('.js-navigation-container')[0]
  focusItem event.currentTarget, container

  return

# Internal: Fire custom navigation:open event.
#
# originalEvent - Original click or keydown jQuery Event
#
# Examples
#
#     $(document).on 'navigation:open', '.listings .listing', ->
#       window.location = $(this).find('a').attr('href')
#       false
#
# Returns nothing.
fireOpen = (originalEvent) ->
  target   = originalEvent.currentTarget
  modifier = originalEvent.modifierKey or
    originalEvent.altKey or originalEvent.ctrlKey or originalEvent.metaKey

  event = $(target).fire 'navigation:open', modifierKey: modifier

  if event.isDefaultPrevented()
    originalEvent.preventDefault()

  return

# Open the item on click.
$(document).on 'click', '.js-active-navigation-container .js-navigation-item', (event) ->
  # Fire custom navigation:open event
  fireOpen event
  return

# Handle keydown open events.
$(document).on 'navigation:keyopen', '.js-active-navigation-container .js-navigation-item', (event) ->
  # Implement js-navigation-open behavior.
  if a = $(this).filter('.js-navigation-open')[0] or $(this).find('.js-navigation-open')[0]
    # If an item contains a js-navigation-open link, navigate to it when the
    # open hotkey is pressed. We don't need to handle this for clicks since
    # thats already part of the browser.

    if event.modifierKey
      window.open a.href, '_blank'
      window.focus()
    else
      $(a).click()

    event.preventDefault()

  else
    # Fire custom navigation:open event
    fireOpen event

  return

# Make container active by adding the
# `js-active-navigation-container` class to it.
#
# container - Element
#
# Returns nothing.
activate = (container) ->
  activeContainer = getActiveContainer()

  unless container is activeContainer
    $(container).fire 'navigation:activate', =>
      $(activeContainer).removeClass 'js-active-navigation-container' if activeContainer
      $(container).addClass 'js-active-navigation-container'

      $(container).fire 'navigation:activated', async: true

# Remove active id from container.
#
# container - Element
#
# Returns nothing.
deactivate = (container) ->
  $(container).fire 'navigation:deactivate', =>
    $(container).removeClass 'js-active-navigation-container'
    $(container).fire 'navigation:deactivated', async: true


containerStack = []

# Active container and push previous onto a stack.
#
# container - Element
#
# Returns nothing.
push = (container) ->
  if activeContainer = getActiveContainer()
    containerStack.push activeContainer

  activate container

  return

# Deactivate container and activate the next on the stack.
#
# container - Element
#
# Returns nothing.
pop = (container) ->
  deactivate container
  clear container

  if activeContainer = containerStack.pop()
    activate activeContainer

  return

# Active the container and focus on the target or first element.
#
# target - Element
# container - Element
#
# Returns nothing.
focus = (target, container) ->
  firstItem = getItems(container)[0]
  item      = $(target).closest('.js-navigation-item')[0] or firstItem

  activate container

  focusPrevented = focusItem item, container
  return if focusPrevented

  if item
    scrollPageTo $(item).overflowParent()[0], item

  return

# Clear current focus.
#
# container - Element
#
# Returns nothing.
clear = (container) ->
  $(container).find('.navigation-focus.js-navigation-item').removeClass 'navigation-focus'
  return

# Clear current focus and focus on the target or first element.
#
# target - Element
# container - Element
#
# Returns nothing.
refocus = (target, container) ->
  clear container
  focus target, container
  return

# Select the previous item and scroll to it if its off screen.
#
# item      - Current navigation item Element
# container - Active navigation container Element
#
# Returns nothing.
cursorUp = (item, container) ->
  items = getItems container
  index = $.inArray item, items

  if previous = items[index-1]
    focusPrevented = focusItem previous, container
    return if focusPrevented

    container = $(previous).overflowParent()[0]

    if getScrollStyle(container) is 'page'
      scrollPageTo container, previous
    else
      scrollItemTo container, previous

  return

# Select the next item and scroll to it if its off screen.
#
# item      - Current navigation item Element
# container - Active navigation container Element
#
# Returns nothing.
cursorDown = (item, container) ->
  items = getItems container
  index = $.inArray item, items

  if next = items[index+1]
    focusPrevented = focusItem next, container
    return if focusPrevented

    container = $(next).overflowParent()[0]

    if getScrollStyle(container) is 'page'
      scrollPageTo container, next
    else
      scrollItemTo container, next

  return

# Scrolls up to the previous item that is above the viewport.
#
# item      - Current navigation item Element
# container - Active navigation container Element
#
# Returns nothing.
pageUp = (item, container) ->
  items = getItems container
  index = $.inArray item, items

  container = $(item).overflowParent()[0]

  # Find the first element that is above the viewport top
  while (previous = items[index-1]) and $(previous).overflowOffset(container).top >= 0
    index--

  if previous
    focusPrevented = focusItem previous, container
    return if focusPrevented
    scrollPageTo container, previous

  return

# Scrolls down to the next item that is below the viewport.
#
# item      - Current navigation item Element
# container - Active navigation container Element
#
# Returns nothing.
pageDown = (item, container) ->
  items = getItems container
  index = $.inArray item, items

  container = $(item).overflowParent()[0]

  # Find the first element that is below the viewport bottom
  while (next = items[index+1]) and $(next).overflowOffset(container).bottom >= 0
    index++

  if next
    focusPrevented = focusItem next, container
    return if focusPrevented
    scrollPageTo container, next

  return

# Handle opening item from keyboard.
#
# Hiting `o` or `enter` on a item will call this method.
#
# item     - .js-navigation-item Element
# modifier - Boolean if modifier key is used
#
# Returns nothing.
keyOpen = (item, modifier = false) ->
  $(item).fire 'navigation:keyopen', modifierKey: modifier
  return


# Focus element by adding the `navigation-focus` class name to it.
#
# target    - Element to focus.
# container - Container Element
#
# Returns Boolean.
focusItem = (target, container) ->
  event = $(target).fire 'navigation:focus', ->
    clear container
    $(target).addClass 'navigation-focus'
    $(target).fire 'navigation:focused', async: true

  event.isDefaultPrevented()

# Gets active container element.
#
# Returns Element.
getActiveContainer = ->
  $('.js-active-navigation-container')[0]

# Get all items in active container.
#
# container - Container Element
#
# Returns Array of Elements.
getItems = (container) ->
  $(container).find('.js-navigation-item').filter(':visible')

# Get scroll style.
#
# container - Container Element
#
# Returns String 'page' or 'item' (default).
getScrollStyle = (container) ->
  $(container).attr('data-navigation-scroll') ? 'item'

# Scrolls page to element if its outside the viewport.
#
# element - Element to scroll to.
#
# Returns nothing.
scrollPageTo = (container, element) ->
  position = $(element).positionedOffset container
  overflow = $(element).overflowOffset container

  # Scroll down
  if overflow.bottom <= 0
    $(container).scrollTo top: position.top - 30, duration: 200

  # Scroll up
  else if overflow.top <= 0
    scrollHeight = if container.offsetParent? then container.scrollHeight else $(document).height()
    scrollTop    = scrollHeight - (position.bottom + overflow.height)
    $(container).scrollTo top: scrollTop + 30, duration: 200

# Scrolls to element if its outside the viewport.
#
# element - Element to scroll to.
#
# Returns nothing.
scrollItemTo = (container, element) ->
  position = $(element).positionedOffset container
  overflow = $(element).overflowOffset container

  # Scroll down
  if overflow.bottom <= 0
    scrollHeight = if container.offsetParent? then container.scrollHeight else $(document).height()
    scrollTop    = scrollHeight - (position.bottom + overflow.height)
    $(container).scrollTo top: scrollTop

  # Scroll up
  else if overflow.top <= 0
    $(container).scrollTo top: position.top


$.fn.navigation = (method) ->
  if method is 'active'
    return getActiveContainer()

  return unless container = $(this).closest('.js-navigation-container')[0]

  methods =
    activate: =>
      activate container
    deactivate: =>
      deactivate container
    push: =>
      push container
    pop: =>
      pop container
    focus: =>
      focus this, container
    clear: =>
      clear container
    refocus: =>
      refocus this, container

  methods[method]?()
