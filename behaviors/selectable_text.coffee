# Selectable Text behavior
#
#= provides js-selectable-text
#
#= require jquery
#
# Makes element the container for triple click selections.
#
# Examples
#
#     <span class="js-selectable-text">
#       50ce349853
#     </span>
#

# Require selection APIs
# IE 8 - no soup for you
return unless window.getSelection?

# Checks element's descendents for target element.
#
# container - Element to check descendents
# target    - Element to check for
#
# Returns Boolean.
containsNode = (container, target) ->
  if container is target
    return true
  for node in container.childNodes when containsNode node, target
    return true
  false

$(document).on 'click', '.js-selectable-text', ->
  selection = window.getSelection()

  if selection.rangeCount
    container = selection.getRangeAt(0).commonAncestorContainer
    # If selection is outside js-selectable-text container
    unless containsNode this, container
      # Clear selection and reselect just the element
      selection.selectAllChildren this

  return
