# Socket Channel subscriptions
#
#= provides js-socket-channel
#
#= require jquery
#= require crema/element/socket
#
# Initializes socket based on defined <link rel=xhr-socket> tag.
#
# THen automatically subscribes elements to socket channels.
#
# ### Markup
#
# ``` html
# <link rel="xhr-socket" href="/_sockets">
# ```
#
# ``` html
# <div class="js-socket-channel" data-channel="issue:123"></div>
# ```
#

# Skip unless sockets are supported
return unless $.fn.socket

# Set mapping of channel name Strings to a Boolean flag
subscribedChannels = {}

# Mapping of channel name Strings to Array of associated Elements
elementChannels = {}

# Internal: Setup channel subscription for element.
#
# link - link[rel=xhr-socket] Element
# el   - js-socket-channel Element
#
# Returns nothing.
subscribeToChannel = (link, el) ->
  socket = $(link).socket()

  return unless names = $(el).attr 'data-channel'

  for name in names.split(' ')
    # Only send a server message if we haven't subscribed yet
    unless subscribedChannels[name]
      socket.send subscribe: name
      subscribedChannels[name] = true

    # Record mapping of channel name to Element
    elementChannels[name] ?= []
    elementChannels[name].push el

  return


# Resubscribe to channels if socket is reopened
$(document).on 'socket:reopen', 'link[rel=xhr-socket]', (event, socket) ->
  for name, _ of subscribedChannels
    socket.send subscribe: name
  return

# Listen for generic socket messages and relay them through the proper
# channels.
$(document).on 'socket:message', 'link[rel=xhr-socket]', (event, message) ->
  [name, data] = message
  return unless name and data
  $(elementChannels[name]).trigger 'socket:message', [data, name]
  return


link = null

$.pageUpdate resubscribeChannels = ->
  return unless link ?= $(document.head).find('link[rel=xhr-socket]')[0]

  # Clear element channels and rescan DOM
  elementChannels = {}

  # Finds all channels on the page and setup the socket subscriptions
  for el in $ '.js-socket-channel'
    subscribeToChannel link, el

  return
