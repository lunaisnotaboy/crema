# Quote Selection Behavior
#
#= require jquery
#= require crema/element/scrollto
#= require crema/events/hotkey
#
# Commenting: Makes it easy to insert selected text into a textarea
# formatted as a blockquote by pressing r when the selection is made
# within an `.js-quote-selection-container` element.
#
# Markup
#
#     <div class="js-quote-selection-container">
#       <div class="comment">
#         <p>
#           What does @dewski think?
#         </p>
#       </div>
#     </div>
#
#     <form>
#       <textarea class="js-quote-selection-target"></textarea>
#     </form>
#

# Select text to reply with blockquote
$(document).on 'keydown', (event) ->
  # Watch for "r" hotkey
  return unless event.hotkey is 'r'

  # Only when the user isn't typing into a field
  return unless event.target is document.body

  selection = window.getSelection()
  selectionNode = $(selection.focusNode)

  # Ignore if the selection isn't made within a quick reply container
  return unless container = selectionNode.closest('.js-quote-selection-container')[0]

  # Ignore if theres no comment field within the container
  return unless field = $(container).find('.js-quote-selection-target:visible')[0]

  # The selected text must be within a quote selection container.
  if selection.toString()
    quotedText = "> #{selection.toString().replace(/\n/g, "\n\> ")}\n\n"

    if currentReplyBoxText = field.value
      quotedText = "#{currentReplyBoxText}\n\n#{quotedText}"

    field.value = quotedText

  $(field).scrollTo duration: 300, ->
    field.focus()
    field.selectionStart = field.value.length

  event.preventDefault()
