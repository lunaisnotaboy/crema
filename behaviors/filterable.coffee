# Filterable Behavior
#
#= provides js-filterable-field
#
#= require jquery
#= require crema/element/fire
#= require crema/element/fuzzy_filter_sort_list
#= require crema/element/prefix_filter_list
#= require crema/element/substring_filter_list
#= require crema/events/throttled_input
#= require crema/polyfills/input
#
# Automatically filters and sorts a list of items against a text field.
#
# ### Markup
#
# * `js-filterable-field`  - Set on field to enable filtering.
# * `data-filterable-for`  - Set to ID of input field to bind too.
# * `data-filterable-type` - Filtering type. Either `"prefix"`, `"substring"`, or `"fuzzy"`. Defaults to `"prefix"`
#
# ``` html
# <input id="file-filter-field" class="js-filterable-field">
#
# <ul data-filterable-for="file-filter-field" data-filterable-type="fuzzy">
#   <li>Foo</li>
#   <li>Bar</li>
#   <li>Baz</li>
# </ul>
# ```
#
# ### Events
#
# `filterable:change`
#
# * **Bubbles** Yes
# * **Cancelable** No
# * **Target** List container Element
# * **Context info**
#   * `relatedTarget` - Input field Element
#
# ``` coffeescript
# $('ul').on('filterable:change', function() {
#   console.log('order changed')
# })
# ```
#

$(document).on 'focusin', '.js-filterable-field', ->
  value = $(this).val()

  $(this).on 'throttled:input.filterable', ->
    return if value is $(this).val()
    value = $(this).val()
    $(this).fire 'filterable:change', async: true

  $(this).on 'blur.filterable', ->
    $(this).off '.filterable'

  $(this).fire 'filterable:change', async: true

  return

# Trigger 'filterable:change' on an input after any
# direct val() calls
$(document).on 'filterable:change', '.js-filterable-field', ->
  query = $.trim $(this).val().toLowerCase()

  for list in $ "[data-filterable-for=#{this.id}]"
    $list = $ list
    filter $list, query
    $list.fire 'filterable:change', relatedTarget: this

  return

# Filter list by query.
#
# list  - List container element
# query - String search query
#
# Returns nothing.
filter = (list, query) ->
  if list.find('.js-filterable-text:first')[0]
    content = (item) ->
      $(item).find('.js-filterable-text:first')[0]

  mark  = list.attr('data-filterable-highlight') isnt undefined
  limit = list.attr 'data-filterable-limit'

  visible = switch list.attr 'data-filterable-type'
    when 'fuzzy'
      list.fuzzyFilterSortList query, {content, mark, limit}
    when 'substring'
      list.substringFilterList query, {content, mark, limit}
    else
      list.prefixFilterList query, {content, mark, limit}

  list.toggleClass 'filterable-active', query.length > 0
  list.toggleClass 'filterable-empty', visible is 0

  return
