# Quick submit
#
#= require jquery
#= require crema/events/hotkey
#= require crema/element/focused_keydown

# Submitting comments with meta+enter and ctrl+enter
$(document).onFocusedKeydown 'textarea', (event) ->
  if event.hotkey is 'meta+enter' or event.hotkey is 'ctrl+enter'
    $(this).closest('form').submit()
    false
