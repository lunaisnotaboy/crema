# Hotkey Behavior
#
#= provides data-hotkey
#
#= require jquery
#= require crema/element/fire
#= require crema/events/hotkey
#= require crema/events/pagechange
#
# Automatically binds hotkeys to any link with a `data-hotkey`
# attribute set. Multiple hotkeys are separated by a `space`.
#
# ### Markup
#
# * `data-hotkey` - String of hotkey combo
#
# ``` html
# <a href="/page/2" data-hotkey="j">Next</a>
# <a href="/help" data-hotkey="ctrl+h">Help</a>
# <a href="/search" data-hotkey="s /">Search</a>
# ```
#
# ### Events
#
# `hotkey:activate`
#
# * **Bubbles** Yes
# * **Cancelable** Yes
# * **Default action** Activates target element
# * **Target** `a` with `[data-hotkey]`
# * **Context info**
#   * `originalEvent` - `keydown` Event
#

# Global mapping String hotkey to target Element.
mappings = {}

$(document).on 'keydown', (event) ->
  # Don't call handler while typing in an input
  return unless event.target is document.body

  if target = mappings[event.hotkey]
    $(target).fire 'hotkey:activate', originalEvent: event, ->
      # Focus target element if its an input
      if $(target).is ':input'
        $(target).focus()

      # Otherwise, act like we clicked it
      else
        $(target).click()

      return

    # Always prevent default action of keydown so nothing is inserted
    event.preventDefault()

  return

# When new html is introduced into the page, we need to rescan
# everything to ensure our mappings index reflects the DOM's current
# state.
$.pageChange remapHotkeys = ->
  mappings = {}
  for el in $('[data-hotkey]')
    hotkeys = $(el).attr('data-hotkey').split(" ")
    for key in hotkeys
      mappings[key] = el
  return
