Behaviors
=========

* MUST pass full page replacement test.
* MUST be implicitly initialized by the presense of `.js-*` class name or `data-*` attribute. No `$.fn.initMenu`.
* SHOULD be purely reactive to user interaction and SHOULD NOT insert additional html on page load.
* MAY lazily create additional html if it is in response to a interactive event. Such as building a popup menu when a button is clicked.
* SHOULD use event delegation over direct element binding.
* SHOULD bind delegated events before `ready` event.
* MAY use direct binding as an optimization in certain cases such as the `mouseover` or `mousemove` events. Additional consideration should be taken to ensure those event listeners are properly rebound on `pageUpdate` and do not double bind themselves.
