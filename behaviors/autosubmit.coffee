# Form AutoSubmit Behavior
#
#= provides data-autosubmit
#
#= require jquery
#
# Automatically submits form when one of its fields change.
#
# ### Markup
#
# ``` html
# <form action="/subscribe" data-autosubmit>
#   <input type=radio name=state value=1> Subscribe
#   <input type=radio name=state value=0> Unsubscribe
# </form>
# ```

$(document).on 'change', 'form[data-autosubmit]', ->
  $(this).submit()
