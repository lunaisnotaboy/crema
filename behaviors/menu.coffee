# Menu Behavior
#
#= provides js-menu-container
#= provides js-menu-target
#= provides js-menu-content
#
#= require jquery
#= require crema/element/fire
#= require crema/element/transition
#= require crema/events/hotkey
#
# * **target** Element with a `.js-menu-target` class name, activates the menu on click.
# * **content** Element with a `.js-menu-content` class name, contains the contents of the menu.
# * **container** Element with a `.js-menu-container` class name, MUST contain only one target and content element.
#
# A menu's activated state is indicated by a `.active` class name on
# the container element.
#
# Clicking on the target activates the menu. Any click outside the
# active menu will deactivate it. (ESC may also be used to deactivate it)
#
# ### Examples
#
# ``` css
# .js-menu-container .js-menu-content { display: none; }
# .js-menu-container.active .js-menu-content { display: block; }
# ```
#
# ``` html
# <div class="js-menu-container">
#   <a href="#" class="js-menu-target">Milestones</a>
#
#   <ul class="js-menu-content">
#     <li>1.0</li>
#     <li>1.1</li>
#     <li>1.2</li>
#   </ul>
# </div>
# ```
#
#
# ### Methods
#
# `.menu('activate')`
#
# Activates menu container.
#
# `.menu('deactivate')`
#
# Deactivates menu container.
#
#
# ### Events
#
# `menu:activate`
#
# * **Bubbles** Yes
# * **Cancelable** Yes
# * **Default action** Activate menu
# * **Target** `.js-menu-container` Element
#
# `menu:activated`
#
# * **Bubbles** Yes
# * **Cancelable** No
# * **Target** `.js-menu-container` Element
#
# `menu:deactivate`
#
# * **Bubbles** Yes
# * **Cancelable** Yes
# * **Default action** Deactivate menu
# * **Target** `.js-menu-container` Element
#
# `menu:deactivated`
#
# * **Bubbles** Yes
# * **Cancelable** No
# * **Target** `.js-menu-container` Element
#
# ``` coffeescript
# $('.milestone-menu').on 'menu:activate', ->
#   unless $(this).find('.milestones')[0]
#     return false
#
# $('.milestone-menu').on 'menu:activated', ->
#   $(this).find('input').focus()
# ```
#

# Current active menu container.
#
# Only one menu can be active at once.
#
# Returns .js-menu-container Element or null
activeContainer = null

# Active menu
#
# container - The Element of the container
#
# Returns nothing.
activate = (container) ->
  # Deactivate any open menu
  if activeContainer
    deactivate activeContainer

  $(container).fire 'menu:activate', ->
    # Bind event listeners
    $(document).on 'keydown.menu', onKeyDown
    $(document).on 'click.menu', onDocumentClick

    activeContainer = container
    $(container).performTransition ->
      $(document.body).addClass 'menu-active'
      $(container).addClass 'active'

    # Fire after activate event
    $(container).fire 'menu:activated', async: true

  return

# Deactivate menu
#
# container - The Element of the container
#
# Returns nothing.
deactivate = (container) ->
  $(container).fire 'menu:deactivate', ->
    # Unbind event listeners
    $(document).off '.menu'

    activeContainer = null
    $(container).performTransition ->
      $(document.body).removeClass 'menu-active'
      $(container).removeClass 'active'

    # Fire after deactivate event
    $(container).fire 'menu:deactivated', async: true

  return

# Handle document click event
#
# Deactivates menu if click if outside the active menu container.
#
# event - jquery.Event
#
# Returns nothing.
onDocumentClick = (event) ->
  # Return fast if no menu is activated, nothing to do
  return unless activeContainer

  # Check if the clicked element is inside the active menu container
  unless $(event.target).closest(activeContainer)[0]
    event.preventDefault()
    # If its outside, deactivate the menu
    deactivate activeContainer

  return

# Handle document keydown event
#
# Deactivates menu if ESC key is pressed.
#
# event - jquery.Event
#
# Returns nothing.
onKeyDown = (event) ->
  # Return fast if no menu is activated, nothing to do
  return unless activeContainer

  if event.hotkey is 'esc'
    # This is pretty special casey, but normally clicking outside
    # the content will clear any focused inputs. But keydown events
    # won't. This could cause the activeElement to be a hidden
    # element.
    #
    # Restrict this behavior to inputs within the container.
    if activeContainer in $(document.activeElement).parents()
      document.activeElement.blur()

    event.preventDefault()
    deactivate activeContainer

  return

# Handle container click event
#
# If the menu isn't active, clicks to the target activate the
# menu. If the menu is already active, clicks outside of the content
# deactivate it.
#
# event - jquery.Event
#
# Returns nothing.
$(document).on 'click', '.js-menu-container', (event) ->
  container = this

  # If target is clicked
  if target = $(event.target).closest('.js-menu-target')[0]
    event.preventDefault()

    # And the menu is already active
    if container is activeContainer
      deactivate container
    else
      activate container

  # If content is clicked
  else if content = $(event.target).closest('.js-menu-content')[0]
    # do nothing

  # Elsewhere in the container is clicked
  else if container is activeContainer
    event.preventDefault()
    deactivate container

  return

# Handle close click event
#
# Deactivate menu if ".close" element is clicked.
#
# event - jquery.Event
#
# Returns nothing.
$(document).on 'click', '.js-menu-container .js-menu-close', (event) ->
  deactivate $(this).closest('.js-menu-container')[0]
  event.preventDefault()
  return


$.fn.menu = (method) ->
  container = $(this).closest('.js-menu-container')[0]

  methods =
    activate: =>
      activate container
    deactivate: =>
      deactivate container

  methods[method]?()
