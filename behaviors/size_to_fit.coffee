# Size to Fit textarea behavior
#
#= provides js-size-to-fit
#
#= require jquery
#= require crema/element/overflow_offset
#= require crema/element/textarea_contents_height
#= require crema/events/pageupdate
#
# Auto sizes any textareas marked with `.js-size-to-fit` to its text
# contents height.

# Resize and install auto resizing listeners.
#
# textarea - The textarea Element
#
# Returns nothing.
install = (textarea) ->
  $textarea = $ textarea
  return if $textarea.data 'size-to-fit-installed'

  userResized = false
  lastHeight = $textarea.height()
  lastInlineHeight = textarea.style.height
  minHeight = parseInt $textarea.css 'min-height'

  resize = (height) ->
    $textarea.height height
    lastHeight = $textarea.height()
    lastInlineHeight = textarea.style.height
    return

  reset = ->
    return unless $textarea.is ':visible'
    resize $textarea.textareaContentsHeight()
    return

  $textarea.closest('form').on 'reset', ->
    # Wait a tick till form is actually cleared
    setTimeout reset, 100

  $textarea.on 'change', ->
    reset()

  $textarea.on 'focus', ->
    # Do reset async
    setTimeout reset, 0

  $textarea.on 'input', ->
    # Skip resizing if user manually resized the form
    return if userResized

    # If the last height we automatically set is different,
    # the user must have changed it themselves.
    if lastInlineHeight isnt textarea.style.height
      userResized = true
      return

    contentsHeight = $textarea.textareaContentsHeight()

    # Ensure theres at least 100px of padding between the bottom of
    # the textarea and the bottom of the viewport.
    if contentsHeight > lastHeight and $textarea.overflowOffset().bottom <= 100
      return

    # Only resize if height has changed
    if contentsHeight isnt lastHeight and contentsHeight > minHeight
      resize contentsHeight

    return

  reset()

  $textarea.data 'size-to-fit-installed', true


$.pageUpdate installSizeToFitListeners = ->
  for textarea in $(this).find '.js-size-to-fit'
    install textarea
  return
