# Commafy
#
#= provides $.commafy
#
#= require jquery
#
# Formats Number with comma delimiters.
#
# Examples
#
#     $.commafy(1000000)
#     # => "1,000,000"
#

# Public: Format number with commas.
#
# num - Number or String to format.
#
# Returns formatted String.
$.commafy = (num) ->
  "#{num}".replace /(^|[^\w.])(\d{4,})/g, ($0, $1, $2) ->
    $1 + $2.replace /\d(?=(?:\d\d\d)+(?!\d))/g, "$&,"
