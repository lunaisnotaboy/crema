# XHR Socket
#
#= provides $.socket
#
#= require jquery
#= require sprockets-modernizr/network-eventsource
#

# Skip unless browser supports SSE
return unless Modernizr.eventsource

# Disable socket on iPhone since the majority of users are on "slow
# connections". This isn't always true, but its the best we can do
# for now. Eventually, once something like `navigator.connection` is
# standarized, we can use that to disable the poller on shit
# connections.
return if navigator.userAgent.match /iPhone/

# Fake WekSocket abstraction to provide a duplex socket over
# SSE + XHR.
#
# An initial handshake is POSTed to the base url which returns a
# connection url in the Location header.
#
# That url can then be connected to over the SSE protocol to receive
# new messages.
#
# The client can sent messages to the server by POSTing to the
# connection url with a message= parameter. This can be used to
# subscribe to specific event channels.
class XhrSocket
  CONNECTING: 0
  OPEN: 1
  CLOSED: 2

  # Establish connection.
  #
  # Response should return a connection id in the Location header.
  # We'll pull that connection url for new messsages.
  constructor: (@base) ->
    @readyState = @CONNECTING
    @listeners = {}
    setTimeout @setup, 0

  # Internal: Send initial handshake
  #
  # Returns nothing.
  setup: =>
    if message = @popMessages()
      data = {message}

    $.ajax
      type: 'POST'
      url: @base
      data: data
      success: (data, status, xhr) =>
        # @url is our connection url
        @url = xhr.getResponseHeader 'Location'

        # Close if server didn't set a Location
        return @close() unless @url

        # Mark OPEN
        @readyState = @OPEN

        # Trigger open event.
        @fire 'open'

        # Firing 'open' may have triggered a callback that closed the socket
        return unless @readyState is @OPEN

        # Flush any pending messages and start the SSE connection.
        @flush()
        @start()

      error: =>
        @close()

    return

  # Start SSE connection
  start: ->
    @source = new EventSource @url
    @source.addEventListener 'message', (event) =>
      message = JSON.parse event.data
      @fire 'message', message
      return
    @source.addEventListener 'reopen', (event) =>
      @fire 'reopen'
      return
    @source.addEventListener 'error', (event) =>
      if @source.readyState is EventSource.CLOSED
        @close()
      return
    return

  # Public: Register event listener.
  #
  # type     - String event type
  # listener - Function callback
  #
  # Returns XhrSocket receiver.
  on: (type, listener) ->
    @listeners[type] ?= []
    @listeners[type].push listener
    this

  # Internal: Fire event.
  #
  # type - String event type
  # args - Optional args to apply to listener callback
  #
  # Returns nothing.
  fire: (type, args...) ->
    return unless listeners = @listeners[type]
    for listener in listeners
      listener.apply this, args
    return

  # Public: Permanently close the socket.
  #
  # Returns nothing.
  close: ->
    @readyState = @CLOSED
    @source?.close()
    @source = null
    @url = null
    @fire 'close'
    return

  # Public: Send message to server.
  #
  # msg - JSON serialiable object.
  #
  # Returns nothing.
  send: (msg) ->
    @outbox ?= []
    @outbox.push msg
    @fire 'send', msg
    @flushTimeout ?= setTimeout @flush, 0 if @readyState is @OPEN
    return

  # Internal: Flush any pending messages.
  #
  # These messages get queued up if they are sent before the socket
  # connection is established
  #
  # Returns nothing.
  flush: =>
    return unless @url
    @flushTimeout = null
    if message = @popMessages()
      $.ajax
        type: 'POST'
        url: "#{@url}/message"
        data: {message}
        error: => @close()
    return

  # Internal: Get messages from outbox and clear it.
  #
  # Returns Array of messages Objects.
  popMessages: ->
    return unless @outbox
    messages = @outbox
    @outbox = null
    messages

# Construct a new XhrSocket socket from a base url.
#
# url - Base URL string
#
# Returns XhrSocket instance.
$.socket = (url) ->
  new XhrSocket url
