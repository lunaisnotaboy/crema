# capitalize
#
#= provides $.capitalize
#
#= require jquery
#
# Capitalizes first letter in every word contained in a string
#
# Examples
#
#     $.capitalize("the rent is too damn high")
#     # => "The Rent Is Too Damn High"
#

# Public: Capitalize the first letter of each word of a string
#
# str - The string to capitalize
#
# Returns capitalized string.
$.capitalize = (str) ->
  str.replace /\w+/g, (word) ->
    "#{word[0].toUpperCase()}#{word.slice(1).toLowerCase()}"