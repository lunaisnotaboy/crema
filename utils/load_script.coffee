# loadScript
#
#= provides $.loadScript
#
#= require jquery
#
# Simple cached script loader.
#
# Examples
#
#     $.loadScript "/foo.js", ->
#       console.log "done"
#

# Public: Load script.
#
# url      - String url
# callback - Function to invoke after script loads
#
# Returns nothing.
$.loadScript = (url, callback) ->
  script        = document.createElement 'script'
  script.src    = url
  script.onload = callback

  head = document.getElementsByTagName('head')[0]
  head.appendChild script
  return
