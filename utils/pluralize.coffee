# pluralize
#
#= provides $.pluralize
#
#= require jquery
#
# Given a string and a positive int count, do a simple pluralization
# (i.e., add an 's'.)
#
# Examples
#
#     "You have #{number_of_commits} #{$.pluralize(number_of_commits, 'commit')}
#     # => "You have 1 commit"
#     # => "You have 3 commits"
#

# Public: Do a simple pluralization on a string.
#
# count - The integer count to check
# base- The string to (maybe) pluralize#
#
# Returns a possibly pluralized string.
$.pluralize = (count, base) ->
  base + (if count > 1 || count == 0 then 's' else '')
