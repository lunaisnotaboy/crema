# Ajax Poller
#
# DEPRECATED
#
#= provides $.ajaxPoll
#
#= require jquery
#
# Replaces the classic [Smart Poller](https://github.com/blog/467-smart-js-polling).
#
# Standard feature set:
#
# * Autoretry with 202 response code convention
# * Time decay - interval increases after each poll
# * Timeout - kill poller after a number of seconds
# * Header version - use as a kill switch on the server side
# * Paused when other ajax requests are sent
#
# Example
#
#     $.ajaxPoll
#       url: url
#       success: (data) ->
#         if data
#           doSomething data
#         else
#           # NOTE: A 202 response would auto resume
#           this.resume()
#

# Public: Initialize and start poller.
#
# options - Standard $.ajax options plus:
#   interval - Number of seconds between requests.
#   timeout  - Number of seconds till poller completely stops.
#   decay    - Number to multiple interval by after each poll.
#
# Returns Poller object.
$.ajaxPoll = (options) ->
  new Poller options

# Public: Increment this value to bust any live pollers.
$.ajaxPoll.version = '1'


class Poller
  interval: 1 # 1s
  timeout: 10 * 60 # 10m
  decay: 1.1

  constructor: (options) ->
    @original = $.extend {}, $.ajaxSettings, options
    @options = $.extend {}, options,
      beforeSend: @onBeforeSend
      success:    @onSuccess
      error:      @onError
      complete:   @onComplete
      cache:      false
      poller:     this

    if options.interval?
      @interval = options.interval
      delete options.interval

    if options.decay?
      @decay = options.decay
      delete options.decay

    if options.timeout?
      @timeout = options.timeout
      delete options.timeout

    # Convert from secs to ms
    @interval = @interval * 1000
    @timeout  = @timeout * 1000

    @start()

  # Public: Start the poller
  #
  # Shouldn't need to call this, should be automatically started.
  #
  # Returns nothing.
  start: =>
    $(document).on 'ajaxStart', @onAjaxStart
    $(document).on 'ajaxStop',  @onAjaxStop
    @started = new Date
    @count = 0
    @resume(true)
    return

  # Public: Permanently stop the poller.
  #
  # Only do this if you don't plan to start it again.
  #
  # Returns nothing.
  stop: =>
    $(document).off 'ajaxStart', @onAjaxStart
    $(document).off 'ajaxStop',  @onAjaxStop
    @pause()
    @started = null
    return

  # Public: Resume poller.
  #
  # immediate - Boolean to schedule XHR to be sent immediately
  #            (default: false)
  #
  # Returns nothing.
  resume: (immediate = false) =>
    return unless @started

    @paused = false

    # Skip if request is already scheduled
    return if @timer

    # Skip if pending XHR is running
    return if @xhr?.readyState < 4

    if immediate
      @poll()
    else
      @timer = setTimeout @poll, @interval

  # Public: Pause poller.
  #
  # Pauses poller, cancels and clears any scheduled requests.
  #
  # Returns nothing.
  pause: =>
    return if !@started || @paused

    @paused = true
    clearTimeout @timer if @timer
    @xhr.abort() if @xhr?.readyState < 4
    @timer = @xhr = null
    return

  # Internal: Handler for timeout function.
  #
  # Schedules new XHR request to be sent after the interval.
  poll: =>
    # Skip if poller is stopped or temporary paused
    return if !@started or @paused

    # Stop poller if timeout is reached
    elapsed = (new Date) - @started
    if elapsed > @timeout
      @stop()
      return

    # If another ajax request is currently running, reschedule ourselves
    if @ajaxStart
      @timer = setTimeout @poll, @interval
      return

    @timer = null
    @count++
    @xhr = $.ajax @options

  # Internal: Handle XHR before send callback.
  #
  # Set version XHR request header. Makes it easy to identify and
  # stop old clients from polling.
  onBeforeSend: (xhr) =>
    xhr.setRequestHeader 'X-Poller-Version', $.ajaxPoll.version
    @original.beforeSend.apply this, arguments if @original.beforeSend

  # Internal: Handle XHR success callback.
  #
  # Increase the check interval by the decay option and schedule
  # another poll timer.
  onSuccess: (data, status, xhr) =>
    if xhr.status is 202
      @interval = @interval * @decay
      @resume()
    else
      @pause()

    @original.success.apply this, arguments if @original.success

  # Internal: Handle XHR error callback.
  #
  # Stop immediately if theres a server error. We're probably only
  # going to cause more trouble if we repeat the request.
  onError: =>
    @pause()
    @original.error.apply this, arguments if @original.error

  # Internal: Handle XHR complete callback.
  onComplete: =>
    @original.complete.apply this, arguments if @original.complete

    # Fully stop poller if none of the callbacks decided to resume it.
    @stop() if @paused

    @xhr = null

  # Internal: Record when other ajax requests start.
  onAjaxStart: =>
    @ajaxStart = true
    return

  # Internal: Record when other ajax requests finish.
  onAjaxStop: =>
    @ajaxStart = false
    return
