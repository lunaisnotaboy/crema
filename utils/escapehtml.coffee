# escapeHTML
#
#= provides $.escapeHTML
#
#= require jquery
#
# String escapes HTML entities.
#
# Examples
#
#     $.escapeHTML("<script></script>")
#     # => "&lt;script&gt;&lt;&#x2F;script&gt;"
#
# See Also
#
# http://benv.ca/2012/10/4/you-are-probably-misusing-DOM-text-methods/
#

# Public: Escape HTML entities.
#
# str - String to escape
#
# Returns escaped String.
$.escapeHTML = (str) ->
  str.replace(/&/g, "&amp;")
    .replace(/</g, "&lt;")
    .replace(/>/g, "&gt;")
    .replace(/"/g, "&quot;")
    .replace(/'/g, "&#039;")
    .replace(/\//g, "&#x2F;")
