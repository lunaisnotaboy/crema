# pageChange Event
#
#= provides .pageChange
#
#= require jquery
#
# `pageChange` is an event hook for rerunning initialization code
# after transitioning to a new page. This is usually in conjunction
# with a call to pushState or replaceState. It runs automatically on
# `ready` and after all pjax page transitions. It maybe triggered
# manually if you use pushState independent of pjax.
#
# Also see pageChange's more specific cousin, pageUpdate. Where
# pageUpdate is designed to be trigged on all new fresh html inserts,
# pageChange is more broadly coupled with large page changes when the
# url changes.
#
# * **Synchronicity** Sync
# * **Bubbles** Yes
# * **Cancelable** No
# * **Target** Element of the page container that was changed
#
# ### Observing
#
# Prefer using `$.pageChange`. `event.target` inside the handler will be
# the container the event was fired from. In the case of `ready`, it
# will be `document.body`. This allows you to scope your selectors to
# the region of the page that actually changed.

handlers = []

# Public: Install global pageChange handler on document.
#
# handler - Function handler to invoke
#
# Returns nothing.
$.pageChange = (handler = ->) ->
  handlers.push handler
  return

# Public: Dispatch hooks useful for logging
$.pageChange.before = ->
$.pageChange.beforeEach = ->
$.pageChange.after = ->
$.pageChange.afterEach = ->

# Public: Trigger pageChange event from element.
#
# Returns chainable jQuery object.
$.fn.pageChange = ->
  for el in this
    $.pageChange.before el
    for handler in handlers
      $.pageChange.beforeEach el, handler
      handler.apply el
      $.pageChange.afterEach el, handler
    $.pageChange.after el
  this

# Bind initial pageChange event to ready handler
$(document).ready triggerInitialPageChange = ->
  $.fn.pageChange.call [document.body]

# Run pageChange after all pjax loads
$(document).on 'pjax:end', triggerPjaxPageChange = (event) ->
  $.fn.pageChange.call [event.target]
