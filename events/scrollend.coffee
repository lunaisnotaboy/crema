# scrollend Event
#
#= provides scrollend
#
#= require jquery
#= require crema/element/fire
#
# Fires when the user finished scrolling.
#
# For performance reasons, expensive handlers should not be ran on
# each `scroll` event. `scrollend` fires asynchronously 100ms after
# the user stopped scrolling.
#
# * **Synchronicity** Async
# * **Bubbles** No
# * **Cancelable** No
# * **Target** Scrollable Element
# * **Context info**
#   * `originalEvent` - Last `scroll` Event
#
# ``` coffeescript
# $(window).on 'scrollend', ->
#   if $(window).scrollTop() + $(window).height() is $(document).height()
#     loadNextPage()
# ```
#

# References
#
# * http://ejohn.org/blog/learning-from-twitter/

# Install global scroll listener once theres someone listening.
setup = ->
  element    = this
  timeoutId  = null
  lastScroll = null

  # Invoked 100ms after scrolling stopped
  checkScroll = ->
    if lastScroll
      $(element).fire 'scrollend',
        bubbles: false,
        originalEvent: lastScroll
      clearTimeout timeoutId
      lastScroll = timeoutId = null
    return

  # Fast scroll handler
  $(element).on 'scroll.scrollend', (event) ->
    # Defer checkScroll time
    clearTimeout timeoutId if timeoutId
    # Record last scroll event
    lastScroll = event
    # Reschedule checkScroll in 100ms
    timeoutId = setTimeout checkScroll, 100
    return

  return

# Uninstall global scroll listener if everyone stops listening.
teardown = ->
  $(this).off 'scroll.scrollend'
  return

$.event.special['scrollend'] = {setup, teardown}
