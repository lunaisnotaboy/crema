# Drag Events
#
#= provides drag:out
#= provides drag:over
#= provides drag:enter
#= provides drag:leave
#
#= require jquery
#= require sprockets-modernizr
#= require crema/element/fire
#
# Makes dragging events work more like the mouse event family.
#
# ### Events
#
# `drag:over`
#
# Fired when dragging enters an Element.
#
# * **Synchronicity** Sync
# * **Bubbles** Yes
# * **Cancelable** No
# * **Target** Element the drag is currently entering.
# * **Context info**
#   * `originalEvent` - `drop` Event
#   * `dataTransfer ` - `dataTransfer` from `drop` Event
#   * `relatedTarget` - Element drag just exited
#
# `drag:out`
#
# Fired when dragging exits an Element.
#
# * **Synchronicity** Sync
# * **Bubbles** Yes
# * **Cancelable** No
# * **Target** Element the drag is currently leaving.
# * **Context info**
#   * `originalEvent` - `drop` Event
#   * `dataTransfer`  - `dataTransfer` from `drop` Event
#   * `relatedTarget` - Element drag just entered
#
# `drag:enter`
#
# Fired when dragging enters the current target.
#
# * **Synchronicity** Sync
# * **Bubbles** No
# * **Cancelable** No
# * **Target** Element the drag is currently entering.
# * **Context info**
#   * `originalEvent` - `drop` Event
#   * `dataTransfer`  - `dataTransfer` from `drop` Event
#   * `relatedTarget` - Element drag just exited
#
# `drag:leave`
#
# Fired when dragging leaves the current target.
#
# * **Synchronicity** Sync
# * **Bubbles** No
# * **Cancelable** No
# * **Target** Element the drag is currently leaving.
# * **Context info**
#   * `originalEvent` - `drop` Event
#   * `dataTransfer`  - `dataTransfer` from `drop` Event
#   * `relatedTarget` - Element drag just exited
#
# ``` coffeescript
# $('#dropzone').on 'drag:enter', ->
#   $(this).addClass "active"
#
# $('#dropzone').on 'drag:leave', ->
#   $(this).removeClass "active"
# ```
#

# Skip unless we have drag events
return unless Modernizr.draganddrop

dragoverTimeout = null
lastTarget      = null
lastCanceled    = false

# Handle dragover event.
#
# Triggers smarter drag:over event as target element changes.
#
# event- jQuery Event
#
# Returns nothing.
onDragOverHandler = (event) ->
  target = event.target

  # If target changed, fire new events
  if target isnt lastTarget
    lastCanceled = fireDragEvents event, lastTarget, target

  # Last last target for comparsion
  lastTarget = target

  if lastCanceled
    event.preventDefault()

  clearTimeout dragoverTimeout

  # If drag event times out, assume its outside the window
  onDragOverTimeout = ->
    lastCanceled = fireDragEvents event, lastTarget, null
    lastTarget = null

  dragoverTimeout = setTimeout onDragOverTimeout, 100

  return

# Fire smart dragout and dragover events from target.
#
# event - jQuery Event
# from  - Element drag exited from
# to    - Element drag entered
#
# Returns Boolean.
fireDragEvents = (event, from, to) ->
  cancel = true

  if from
    $(from).fire 'drag:out',
      originalEvent: event
      dataTransfer:  event.originalEvent.dataTransfer
      relatedTarget: to

  if to
    $(to).fire 'drag:over',
      originalEvent: event
      dataTransfer:  event.originalEvent.dataTransfer
      relatedTarget: from
    , -> cancel = false

  cancel


dragOverCount = 0

# Install global dragover listener once theres someone listening for
# drag:out or drag:over events.
setup = ->
  if ++dragOverCount is 1
    $(window).on 'dragover', onDragOverHandler
  return

# Uninstall global dragover listener if everyone stops listening.
teardown = ->
  if --dragOverCount is 0
    $(window).off 'dragover', onDragOverHandler
  return

# Handle enter and leave events by checking if the last target is
# still inside the target container. So if the drag event moved to a
# new element outside the container target.
#
# Returns event handler result.
handle = (event) ->
  target = this
  {type, relatedTarget, handleObj} = event

  if !relatedTarget or (relatedTarget isnt target and !$.contains(target, relatedTarget))
    event.type = handleObj.origType
    result = handleObj.handler.apply this, arguments
    event.type = type

  result


$.event.special['drag:out']  = {setup, teardown}
$.event.special['drag:over'] = {setup, teardown}

$.event.special['drag:enter'] = {handle, delegateType: 'drag:over', bindType: 'drag:over'}
$.event.special['drag:leave'] = {handle, delegateType: 'drag:out',  bindType: 'drag:out'}
