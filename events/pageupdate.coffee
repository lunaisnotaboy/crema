# pageUpdate Event
#
#= provides .pageUpdate
#
#= require jquery
#
# `pageUpdate` is an event hook for rerunning initialization code
# after new html is inserted or changed. For an example, you may need
# to rebind event observers on new html elements or rewrite HTML in
# JS. It runs automatically on `ready` and after pjax responses but
# must be triggered by hand in other cases.
#
# * **Synchronicity** Sync
# * **Bubbles** Yes
# * **Cancelable** No
# * **Target** Element of the region that has updated
#
# ### Triggering
#
# You'll want to trigger `pageUpdate` anytime you insert new html into
# the DOM. Is this mostly after AJAX requests.
#
# ``` coffeescript
# $.ajax
#   success: (html) ->
#   $('#site').html(html).pageUpdate()
# ```
#
# Avoid triggering pageUpdate multiple times. If you update multiple
# regions, only trigger pageUpdate once on a common parent element.
#
# ``` coffeescript
# $.ajax
#   success: (data) ->
#     $('#comments').append(data.comment)
#     $('#header').append(data.header)
#     $('#container').pageUpdate()
# ```
#
# ### Observing
#
# Prefer using `$.pageUpdate`. `event.target` inside the handler will be
# the container the event was fired from. In the case of `ready`, it
# will be `document.body`. This allows you to scope your selectors to
# the region of the page that actually changed.
#
# ``` coffeescript
# # Rewrite any `js-relative-date` elements
# $.pageUpdate ->
#   $(this).find('.js-relative-date').relativizeDate()
# ```
#
# If you do make any small changes to the DOM within the handler,
# avoid retriggering pageUpdate so it doesn't lead to an infinite
# recursion situation.
#
# Be care to ensure your handler can safely run multiple on the same
# elements. This naive handler my cause multiple `mouseover` elements
# to be installed. DO NOT do something like this:
#
# ``` coffeescript
# $.pageUpdate ->
#   $('#issues').on 'mouseover', ->
# ```
#

handlers = []

# Public: Install global pageUpdate handler on document.
#
# handler - Function handler to invoke
#
# Returns nothing.
$.pageUpdate = (handler = ->) ->
  handlers.push handler
  return

# Public: Dispatch hooks useful for logging
$.pageUpdate.before = ->
$.pageUpdate.beforeEach = ->
$.pageUpdate.after = ->
$.pageUpdate.afterEach = ->

# Public: Trigger pageUpdate event from element.
#
# Returns chainable jQuery object.
$.fn.pageUpdate = ->
  for el in this
    $.pageUpdate.before el
    for handler in handlers
      $.pageUpdate.beforeEach el, handler
      handler.apply el
      $.pageUpdate.afterEach el, handler
    $.pageUpdate.after el
  this

# Bind initial pageUpdate event to ready handler
$(document).ready triggerInitialPageUpdate = ->
  $.fn.pageUpdate.call [document.body]

# Run pageUpdate after any pjax requests
$(document).on 'pjax:complete', triggerPjaxPageUpdate = (event) ->
  $.fn.pageUpdate.call [event.target]
