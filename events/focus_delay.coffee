# Delayed Focus Events
#
#= provides focusin:delay
#= provides focusin:delayed
#= provides focusout:delay
#= provides focusout:delayed
#
#= require jquery
#= require crema/element/fire
#
# Provides a family cancelable focus/blur that trigger slightly later.
# This makes it easier to build menus that only display when you are
# focused in on a field.
#
# ### Events
#
# `focusin:delay`
#
# Fired when field is being focused for the first time. Will not fire
# if field is blurred, and refocused in less than 200ms.
#
# * **Synchronicity** Sync
# * **Bubbles** Yes
# * **Cancelable** Yes
# * **Target** Element that is receiving focus.
#
# `focusin:delayed`
#
# Fired when field has been focused for the first time.
#
# * **Synchronicity** Sync
# * **Bubbles** Yes
# * **Cancelable** No
# * **Target** Element that is receiving focus.
#
# `focusout:delay`
#
# Fired when field is being blurred. Will always fired when field is
# blurred. Canceling can will prevent it.
#
# * **Synchronicity** Sync
# * **Bubbles** Yes
# * **Cancelable** Yes
# * **Target** Element that is receiving focus.
#
# `focusout:delayed`
#
# Fired when field has been blurred for the first time. Won't fire if
# the field is not refocused in under 200ms or the `focusout:delay` is
# canceled.
#
# * **Synchronicity** Sync
# * **Bubbles** Yes
# * **Cancelable** No
# * **Target** Element that is receiving focus.
#
# ``` coffeescript
# $('input').on 'focusin:delayed', ->
#   showAutocompletions()
#
# $('input').on 'focusout:delayed', ->
#   hideAutocompletions()
# ```
#

$(document).on 'focusin.delay', (event) ->
  target = event.target
  unless $.data target, 'focus-delay-active'
    $(target).fire 'focusin:delay', ->
      $.data target, 'focus-delay-active', true
      $(target).trigger 'focusin:delayed'
      return
  return

$(document).on 'focusout.delay', (event) ->
  setTimeout ->
    target = event.target
    if target isnt document.activeElement
      $(target).fire 'focusout:delay', ->
        $.removeData event.target, 'focus-delay-active'
        $(target).trigger 'focusout:delayed'
        return
    return
  , 200
