Events
======

Provides additional custom events.

* SHOULD have no side effects of their own. Allowing for an optimization when no event listeners are installed, the internal handler itself would not need to run.
* SHOULD use $.event.special to install lifecycle hooks.
