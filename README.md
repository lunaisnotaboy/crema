# Crema

A collection of GitHub's finest JS behaviors and components.


## Installation

### Rails

Add `https://github.com/github/crema.git` to your app's `bower.json` at `vendor/assets/bower.json`.

``` json
{
  "name": "myapp",
  "dependencies": {
    "crema": "https://github.com/github/crema.git#0.x.x"
  }
}
```

Make sure you have `>= 1.1.0` of bower:

```
$ npm install -g bower
$ bower -v
1.1.0
```

To pull down the package and any updates, `cd` into `vendor/assets` and run `bower install`.

```
$ cd vendor/assets
$ bower install
```

You should check in `bower.json` and everything under `vendor/assets/bower_components`.

**Rails 3.x Note** Rails 3.x is locked to an older version of sprockets that doesn't support bower. You can work around this by installing the [sprockets 2.2.2.backport2](http://rubygems.org/gems/sprockets/versions/2.2.2.backport2) gem.

``` ruby
gem 'sprockets', '2.2.2.backport2'
```

## Requiring

Every file is designed to be individually cherry picked. There is no single `require crema` that would bring in everything.

``` coffeescript
#= require crema/events/pageupdate
#= require crema/behaviors/menu
```

Internally, this means each individual module MUST state all of its dependencies, yes even jQuery.

## Provides

All modules SHOULD have a `provides` directive that states any symbols it exports.

``` coffeescript
# Exports $.filter function
#= provides $.filter
#
# Exports $.fn.filter function
#= provides .filter
#
# Automatically binds to data-filterable attribute
#= provides data-filterable
#
# Automatically binds to .js-filter class name
#= provides js-filter
```

These directives can be used for scanning application view templates and javascript files and suggest a list of modules to require.

## Categories

* [**Behaviors**](behaviors) Unobtrusive reactive widgets or handlers.
* [**Element**](element) Property extensions to `$.fn`.
* [**Events**](events) Additional custom events.
* [**Filters**](filters) Hooks that rewrite the DOM on `pageUpdate`.
* [**Polyfills**](polyfills) Backports for older browsers.
* [**Utils**](utils) Random functions that expose themself globally or on `$`.

## Style

* Use soft-tabs with a two space indent.
* Always use camelCase, never underscores.
* Use implicit parentheses when possible.
* Defer to [@jashkenas](https://github.com/jashkenas)'s style. See the [CoffeeScript documentation](http://jashkenas.github.com/coffee-script/) for good examples.

## Full Page Replacement Test

If the entire body html is replaced, it will regenerate all new DOM elements and cause all directly bound event listeners to be lost. Most page functionality will be broken. This type of thing happens with pjax.

A quick way to simluate this is with this script:

``` javascript
$('body').html($('body').html())
$('body').pageUpdate()
```

If our javascript is well written, it will work even after. This means using techniques like event delegation or hooking into `pageUpdate` to reinitialize code.

## Developing

* [Running and writing tests](test#readme)

### Changes Checklist

* Check individual rule list in module directory. Example: [crema/behaviors/README.md](/behaviors/README.md)
* Keep inline documentation up to date
* Ensure module has proper `provides` and `require` directives
* Add or update existing [unit](/test/unit) or [functional](/test/functional) tests
* Add note to [CHANGELOG.md](/CHANGELOG.md) for significant changes
