* Convert pageUpdate and pageChange to simple handler lists than full events
* Deprecate ajaxPoll util
* Added quote selection behavior
* Removed textchange event
* Removed simulate click helper
* Removed js-activate-link behavior
* Deprecate js-activate-link behavior
* Merged js-navigation-target behavior into js-navigation-item
* Changed $.fn.socket require to crema/element/socket
* Add $.fn.socket initializer
* Hide XhrSocket class
* Added onFocusedKeydown helper
* Removed items option for list filters
* Removed js-not-filterable option from filterable behavior
* Added XHR socket poller
* Added escapeHTML util
* Changed replacement email obfuscator to allow html tags
* Added deferred content loader
* Added pageChange event
* Converted from gem to bower package
* Remove tab switching detection from $.ajaxPoll
* Removed hashchange polyfill (drop IE 7/8)
* Added throttled:input event
* Changed data-filterable attr to js-filterable-field class
* Changed js-active-navigation-container from id to class
* Deprecate textchange event
* Added input event polyfill
* Added loadScript util
* Removed vendored Ace
* Renamed overflowContainer to overflowParent
* Added navigation behavior
* Added filterable behavior
* Added commafy util
* Added element simulateClick method
* Added ajaxPoll util
* Removed window.mouseHidden global
* Added element touch
* Added scroll position element methods
* Added unobfuscate email filter
* Added hashchange polyfill
* Added mouse hidden events
* Added activated link behavior
* Change `pageUpdate` to only fire after pjax ajax requests
* Added `scrollend` event
* Added custom dragging events
* Added $.fn.pageUpdate
* Added Element#fire
* Added modernizr gem dependency
* Added rails-behaviors gem dependency
* Added menu behavior
* Added `textchange` event.
* Added relative date filter.
* Added `pageUpdate` event.
* Initial release.
